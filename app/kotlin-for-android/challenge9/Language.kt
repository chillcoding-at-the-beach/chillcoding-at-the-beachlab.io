package com.chill.network.model

data class Language(val name: String)