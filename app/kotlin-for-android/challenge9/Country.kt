package com.chill.network.model

data class Country(val name: String, val capital: String, val languages: List<Language>)