package com.chillcoding.magic

import android.app.Application
import androidx.core.content.res.ResourcesCompat

class App : Application() {

    companion object {
        lateinit var instance: App
        val sColors: List<Int> by lazy {
            listOf(
                ResourcesCompat.getColor(instance.resources, R.color.color_red, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_yellow, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_green, null)
            )
        }
        val rainBowColors: List<Int> by lazy {
            listOf(
                ResourcesCompat.getColor(instance.resources, R.color.color_dark_red, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_dark_orange, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_orange, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_light_yellow, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_green_1, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_green_2, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_light_green, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_light_blue, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_blue, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_dark_purple, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_purple, null),
                ResourcesCompat.getColor(instance.resources, R.color.color_pink, null)
            )
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}