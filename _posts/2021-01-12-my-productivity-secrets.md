---
title: "My productivity secrets"
categories: en chillcoding
author: remy
last_update: 2021-01-12
---

I work 4 days a week, but many people don't even realize that. Following are my productivity secrets.

<!--more-->

## Morning routine

Every morning, my work routine follows a simple most-to-less priority strategy, which looks as follows:

1. [5-30 minutes] I open **Slack** to check if there were **any urgent matters** during my last (approximately) 14 hours of rest.
   Depending on what happened, it can go from "for your information" messages to "`master` is broken".
1. [10-60 minutes] The most priority thing after Slack are **merge requests that I was assigned to review.**
   For that, I check the "my assigned merge requests" GitLab shortcut (top-right corner).
1. [5-15 minutes] After I'm done reviewing merge requests, I check my emails, and **most specifically the ones where me
   or my team were specifically mentioned.**
   Depending on the notifications content, it can take me some time to reply with thoughtful answers.
1. [5-20 minutes] Then I read the rest of the emails (I usually have between 40 and 70 emails in the morning) **until my inbox is empty**.
   Depending on the notifications content, it can take me some time to reply with thoughtful answers.
1. [2-3.5 hours] Planned tasks.
1. When I want to take a break, **I check [my GitLab To-dos](https://gitlab.com/dashboard/todos).**
   To-dos are the list of remaining items that I didn't address by replying to my email notifications or reviewing merge requests.
   It also includes old mentions that I need to reply to, or To-dos that I created myself as reminders for issues I'm interested in.

## Afternoon routine

Usually, in the afternoon I manage to have even more productive time as I don't have a backlog of Slack notifications/emails.

Apart from that, the priority strategy is the same.

## Avoid meetings and get trust from your manager

[The Two Biggest Drags On Productivity: Meetings And Managers](https://www.inc.com/jason-fried/excerpt-easy-on-the-mms.html).

This sums up well my take on meetings and "micro-management". Avoid meetings at all cost, and don't let your manager
check everything that you do, or even tell you everything that you need to do. You're probably the best person to know
what you should be working on!

When I say "avoid meetings at all cost", I mean it. Following are my weekly meetings (at most one 30-minutes meeting per day):

- Monday: 1-on-1 with my manager.
- Tuesday: My team (Engineering Productivity) meeting.
- Wednesday: (Optional) Quality department staff (i.e. managers) meeting.
- Thursday: Quality department meeting.

Each meeting is time-boxed to 30 minutes and rarely last longer. That means I get at least 7 hours for my own work.

## Multi-tasking

My work involves a lot of multi-tasking and I believe that's probably the best productivity trick. We know that after
being interrupted, it can take 15 minutes to get back "in the mental zone" and be able to produce anything.

While it's probably true for big tasks, it's probably not for most tasks. I think tasks can be split into 3 types:

1. Zero to small context tasks: reading Slack, emails, looking at charts etc.
1. Small to medium context tasks: small scope tasks for which you can load the context in your brain in a few seconds.
1. Big context tasks: significant R&D/architecture tasks, tasks that involve a lot of layers, projects etc. for which the
   context loading can take several minutes.

**Iteration is the key here: split tasks or subjects in small parts that your brain can process more easily/quickly.**
Basically, you want to transform a type 3 task (big context) into a type 2 (smaller context).
That way, if you're interrupted at any point, it's easy to get back to it since it's small.

I think, with enough training, you can reduce the number of type 3 tasks significantly. Obviously, that might not apply
depending on your job/position, but for Backend engineers, I think it works well.

## Shortcuts

Depending on your jobs, that might or might not apply, but for my job (backend engineer), it gives me a huge productivity
boost. My work involves doing repetitive tasks such as:

- Creating Git branches. You usually do that with `git fetch origin master && git checkout -b my-branch origin/master`, but with my `git cob` shortcut I just have to type `git cob my-branch`. 🚀
- Creating Git commits, ideally signed with your GPG key. You usually do that with `git commit --gpg-sign --signoff -m "My commit message"`, but with my `git ci` shortcut I just have to type `git ci -m "My commit message"`. 🚀
- Rebasing a Git branch. You usually do that with `git fetch origin master && git rebase origin/master`, but with my `git rom` shortcut I just have to type `git rom`. 🚀
- Pushing a Git branch and opening a merge request. You usually do that with `git push origin` and then go to GitLab and open a new merge request, but with my `git pomr` shortcut I just have to type `git pomr`. 🚀

Imagine I need to make a quick change, that only requires four small steps:

1. Create the Git branch: `git cob my-quick-change`.
1. Perform the actual change in my text editor (Atom).
1. Stage the changes: I use `git ap` as a shortcut for `git add -p`, which allows to interactively reviewing and staging patches.
1. Create a Git commit: `git ci -m "This is a quick change"`.
1. Push the Git branch and create the merge request: `git pomr`.
1. Edit the merge request (add labels, fill its description etc.).

Also, knowing the basics of Shell and Git commands helps quickly navigating, checking status, interactively rebasing etc.

## Friday = beach time 🏝

Thanks to these productivity strategy and tricks, I can spend my Fridays at the beach!

Don't hesitate to share your productivity strategy and tips or ask me questions if you want more details.
