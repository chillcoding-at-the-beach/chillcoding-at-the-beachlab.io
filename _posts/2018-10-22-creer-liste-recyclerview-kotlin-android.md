---
title: "Afficher une liste d'éléments avec un RecyclerView [AK 6 Liste d'éléments]"
categories: fr coding tutoriel android kotlin
author: macha
last_update: 2021-04-7
---

<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/gallery/thumbs/6_android-adapter.svg"
  alt="Liste d'éléments dans l'app. mobile kotlin pour Android" width="200px"/>
</div>

Cet article explique comment afficher la liste des versions <i>Android</i> via un
<b style='color:green'>RecyclerView</b>, en _Android_ avec le langage _Kotlin_.

Le <b style='color:green'>RecyclerView</b> est un composant visuel _Android_
de type conteneur. Ce type de vue permet d'afficher un tableau d'éléments
(ici le tableau des versions _Android_), sous forme de liste ou de grille.
Il fonctionne avec un adaptateur ou <i>adapter</i>.

De manière générale, le concept d'adaptateur en <i>Android</i> permet de faire
le lien entre la vue et les données.

L'implémentation se fait en trois étapes :

  {% icon fa-code %} [Implémentation liée aux données]({{ page.url }}#data)

  {% icon fa-code %} [Implémentation de l'adaptateur]({{ page.url }}#adapter)

  {% icon fa-code %} [Configuration de l'adaptateur avec sa vue]({{ page.url }}#config)

<!--more-->

## Implémentation liée aux données <a name="data"></a>

Tout d'abord, il s'agit de mettre en place l'<b style='color:green'>Activity</b>
principale.

Ensuite, il s'agit de créer une classe représentant nos éléments à afficher. Plus précisément,
il s'agit de créer une classe de donnée représentant une version d'_Android_.
Pour commencer, notre version d'_Android_ ne possède qu'un nom (_name_).

Puis, il s'agit de réaliser les implémentations liées aux données.
En particulier, un tableau d'éléments, soit un tableau de versions _Android_, va
être déclaré dans l'<b style='color:green'>Activity</b> principale, ce dernier
contient les données à afficher.

1. Créez un nouveau projet _Android Studio_, dont l'<b style='color:green'>Activity</b>
principale est **MainActivity**, avec le modèle <b style='color:green'>EmptyActivity</b>.

2. Créez un nouveau fichier _Kotlin_ contenant une _data class_, nommons la **AndVersion** :

    ```kotlin
    data class AndVersion(var name: String)
    ```

    Cette dernière permet de créer des objets de type **AndVersion** ayant un nom,
    "Lollipop", par exemple `val loli = AndVersion("Lollipop")`.

    Note : L'intérêt d'utiliser une classe de donnée est la génération automatique de fonction
    d'affichage tel que _toString()_, de copie d'objet, _copy()_, d'égalité, _equals()_,
    etc [\[1\]](#dataclass).

3. Créez le tableau d'éléments dans la classe **MainActivity** :

    ```kotlin
    val items = arrayOf(AndVersion("Banana"), AndVersion("Lollipop"))
    ```

À présent, les données sont créées, présentes dans le tableau `items`.
Il s'agit, à présent, d'implémenter un adaptateur de données pour le composant visuel
<b style='color:green'>RecyclerView</b>, soit une classe héritant de <b style='color:green'>RecyclerView.Adapter</b>.

Il est aussi possible d'**afficher une liste avec la bibliothèque _Compose_**, retrouvez
les indications dans le thème "Liste d'éléments" de l'app. "Kotlin pour Android".
<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak6-fr.png" alt="liste de plage avec Compose dans l'app. kotlin pour Android"/>
</div>

{% include aside.html %}


## Implémentation de l'adaptateur <a name="adapter"></a>

Dans cette partie, il faut créer la vue d'un élément de la liste, soit la vue d'une
version _Android_. Ensuite, il s'agit de créer un adaptateur pour la liste de versions
Android. En particulier, nous allons créer une classe héritant de
<b style='color:green'>RecyclerView.Adapter</b>.


1. Ajoutez le fichier `item_and_version.xml`, la vue représentant une ligne, dans le dossier `res/layout/` :

    ```xml
    <LinearLayout
        xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:layout_width="match_parent"
        android:layout_height="@dimen/common_size">

        <TextView
            android:id="@+id/andVersionTxt"
            android:layout_width="match_parent"
            android:layout_height="@dimen/common_size" />

        <View
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:background="@color/colorPrimaryDark" />
    </LinearLayout>
    ```

    Remarque : la valeur `match_parent`, pour les attributs `android:layout_width`
    et `android:layout_height`, cela signifie que la hauteur et la largeur doit
    correspondre au parent.

    Remarque : la valeur `common_width` est une dimension à créer dans le fichier _values/dimens.xml_

2. Inclure la bibliothèque du <b style='color:green'>RecyclerView</b> dans le fichier `build.gradle (Module: app)` :

    ```
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    ```

3. Déclarez une classe héritant de <b style='color:green'>RecyclerView.Adapter</b>

    ```kotlin
    class AndVersionAdapter(val items: Array<AndVersion>) : RecyclerView.Adapter<AndVersionAdapter.ViewHolder>() {

    }
    ```

    Remarque : Certains éléments, noms de classe, sont en rouge,
    il s'agit d'importer les classes manquantes (via le raccourcis clavier __alt__ + __Enter__).

4. Ajoutez, dans la classe **AndVersionAdapter**, la composition (la <i>inner class</i>) :

    ```xml
    inner class ViewHolder(val binding: ItemAndVersionBinding) : RecyclerView.ViewHolder(binding.root)
    ```

    Note : il est utilisé le concept de _ViewBinding_ (cf. [AK-4])

5. Ajoutez les méthodes d'héritage pour **AndVersionAdapter** :

   la fonction retournant le nombre d'éléments de la liste :

    ```kotlin
    override fun getItemCount(): Int = items.size
    ```

      la fonction retournant la visualisation d'une ligne de la liste :

    ```kotlin
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
      val binding = ItemAndVersionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
      return ViewHolder(binding)
       }
    ```

    la fonction s'occupant de charger les données dans la vue :

    ```kotlin
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }
    ```
6. Ajoutez, dans le _ViewHolder_ de **AndVersionAdapter**, la fonction qui permet de lier les données à la vue :

    ```xml
            fun bindAndVersion(andVersion: AndVersion) {
                with(andVersion) {
                    bindind.andVersionTxt.text = "$name"
                }
            }
    ```
7. Implémentez alors la méthode de **AndVersionAdapter** de cette façon :

    ```kotlin
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindAndVersion(items[position])
    }
    ```


À présent, l'implémentation de l'adaptateur est complète, il ne reste plus qu'à l'utiliser.

## Configuration de l'adaptateur avec sa vue <a name="config"></a>

À présent, il s'agit de déclarer dans la vue principale le <b style='color:green'>RecyclerView</b>,
`andVersionRecyclerView`. Puis, il s'agira de configurer `andVersionRecyclerView`.
En effet, il a besoin d'un gestionnaire d'agencement, <b style='color:green'>LinearLayoutManager</b>,
ainsi que d'un adaptateur, **AndVersionAdapter**.



1. Déclarez le composant <b style='color:green'>RecyclerView</b> à la racine de la vue principale, **activity_main.xml** :

    ```xml
    <androidx.recyclerview.widget.RecyclerView
        xmlns:android="http://schemas.android.com/apk/res/android"
        android:id="@+id/andVersionRecyclerView"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />

    ```

2. Spécifiez un <b style='color:green'>LinearLayoutManager</b> pour l'objet `andVersionRecyclerView`,
dans la méthode <i style='color:green'>onCreate()</i>, de
l'<b style='color:green'>Activity</b> principale :

    ```kotlin
    andVersionRecyclerView.layoutManager = LinearLayoutManager(this)
    ```

3. Spécifiez l'adaptateur pour l'objet `andVersionRecyclerView`, toujours dans
la méthode <i style='color:green'>onCreate()</i>, de l'<b style='color:green'>Activity</b>
principale :

    ```kotlin
    andVersionRecyclerView.adapter = AndVersionAdapter(items)
    ```

4. Spécifiez à l'adaptateur le changement des données, par exemple dans la méthode <i style='color:green'>onResume()</i> :

    ```kotlin
    andVersionRecyclerView.adapter.notifyDataSetChanged()
    ```

À présent, l'<b style='color:green'>Activity</b> principale **MainActivity** déclare
un <b style='color:green'>RecyclerView</b>, `andVersionRecyclerView`. Ce dernier
est configuré avec l'adaptateur **AndVersionAdapter**.
Ici, vous devriez pouvoir exécuter le projet.

## Plus loin

Il est possible d'utiliser une fonction d'extension pour charger la vue dans
la méthode <i>onCreateViewHolder()</i> de la classe **AndVersionAdapter** :
```kotlin
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
       return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

```
La méthode <i>onCreateViewHolder()</i> devient alors :
```kotlin
override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AndVersionAdapter.ViewHolder {
    return ViewHolder(parent.inflate(R.layout.item_and_version))
}

```

Pour le moment seul le titre de la version s'affiche, il est possible d'ajouter
une image.

Ajoutez l'élément graphique dans le fichier `item_and_version.xml` :
```xml
  <ImageView
      android:id="@+id/andVersionImg"
      android:layout_width="@dimen/common_size"
      android:layout_height="@dimen/common_size" />
```

Ajoutez l'attribut `img` dans la classe **AndVersion** :
```
data class AndVersion(var name: String, var img:Int)
```

Ajoutez les images dans le dossier `res/drawable/`.

Ajoutez le tableau de chaîne de caractères, dans `strings.xml` :
```xml
<string-array name="andVersionName">    
  <item>Cupcake</item>    
  <item>Donut</item>    
  <item>Eclair</item>
</string-array>
```

Ajoutez une fonction <i>seedItems()</i>, son rôle est d'initialiser les tableaux :
```kotlin
var nameArray = resources.getStringArray(R.array.andVersionName)
val imgArray = intArrayOf(R.drawable.cupcake, R.drawable.donut, R.drawable.eclair)
for (i in 0..(nameArray.size-1))
            items[i] = AndVersion(nameArray[i], imgArray[i])

```
`items` devient dynamique, il est initialisé au cours
de l'exécution de l'application (cf. [AK-2] pour initialiser un tableau en Kotlin).
Il est alors initialisé comme suit :
```kotlin
val items = Array<AndVersion>(10, {AndVersion("toto", R.drawable.donut)})
```

Remarque : Le tableau `imgArray` doit être de taille égale (ou supérieure) à `nameArray`
afin que l'index `i` ne dépasse pas les capacités de `imgArray`.

Faites le lien entre la vue d'une ligne et la donnée dans le <b style='color:green'>ViewHolder</b> de l'adaptateur :
```
binding.andVersionImg.setImageResource(img)
```

Il est aussi possible d'intercepter le clique sur l'image depuis le
<b style='color:green'>ViewHolder</b> :
```
binding.andVersionImg.setOnClickListener { itemView.context.toast("$name")}
```

Avec la fonction <i>toast()</i> disponible via la bibliothèque <i>Anko</i> (cf. [AK-4] Utiliser la bibliothèque Anko dans un projet Android).
Il est possible, dans certain cas, que les données de l'adaptateur change.
À ce moment, la méthode <i>notifyDataSetChanged()</i> est utilisée sur l'objet
de type <i>adapter</i>, ici `andVersionRecyclerView.adapter`.




Finalement, en trois quart d'heure top chrono il est possible d'implémenter un
<b style='color:green'>RecyclerView</b>. Cela dit, c'est une implémentation qui
demande à être pratiquée afin d'être parfaitement maîtrisée, selon les trois étapes
d'implémentation :

{% icon fa-code %} [Implémentation liée aux données]({{ page.url }}#data)

{% icon fa-code %} [Implémentation de l'adaptateur]({{ page.url }}#adapter)

{% icon fa-code %} [Configuration de l'adaptateur avec sa vue]({{ page.url }}#config)

L'intérêt d'utiliser un <b style='color:green'>RecyclerView</b> est de pouvoir,
comme son nom l'indique, recycler les lignes à afficher. Ces dernières sont stockées
en cache via le <b style='color:green'>ViewHolder</b>.

La complexité de ce développement réside dans l'appréhension du concept d'adaptateur.
Ce concept est récurrent en <i>Android</i>, il s'applique avec des vues de type conteneur,
comme le <b style='color:green'>GridView</b>, le <b style='color:green'>ViewPager</b>, le <b style='color:green'>Spinner</b>, etc.
Chacune de ces vues possède son type d'adaptateur (<b style='color:green'>RecyclerView.Adaptateur</b>,
<b style='color:green'>FragmentStatePagerAdapter</b>, <b style='color:green'>SpinnerAdapter</b>, etc.).
L'adaptateur peut parfois être utilisé tel quel, sans être hérité comme on l'a fait dans cet article.
Par exemple, avec le <b style='color:green'>Spinner</b>, il suffit d'indiquer un
tableau de chaîne de caractère dans le constructeur de <b style='color:green'>SpinnerAdapter</b>.

En outre, la démonstration de cette liste de version <i>Android</i> est disponible
dans la partie "Version Android" de l'application [Kotlin pour Android sur Play Store](https://play.google.com/store/apps/details?id=com.chillcoding.kotlin).


### {% icon fa-globe %} Références

1. <a name="dataclass"></a>[Documentation Kotlin : Data classes](https://kotlinlang.org/docs/reference/data-classes.html)
2. [Merge Request 6 Item List Adapter](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/14/diffs)
3. [Merge Request 6 Shortcuts List Adapter](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/30/commits)
4. [Antonio Leiva: RecyclerView in Android: The basics](https://antonioleiva.com/recyclerview/)

*[AS]: Android Studio
[AK-2]:https://www.chillcoding.com/blog/2019/09/26/kotlin-array/
[AK-4]:https://www.chillcoding.com/blog/2021/03/30/utiliser-view-binding-kotlin/
