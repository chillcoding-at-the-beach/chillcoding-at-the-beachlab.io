---
title: "Comment gérer ses communications ?!"
categories: fr chillcoding
author: macha
last_update: 2020-09-09
---

«
— Dring, dring, dring!

— Je veux pas répondre, mon appel en cours est passionnant !

— Ding!

— Yes, je confirme l'aprem plage par SMS.

— Dong, dong, dong!

— Ça y va les messages _Facebook_ !

— Dong, dong!

— Oh mince ! J'ai plus de batterie...»

<span Style="font-variant-caps:small-caps">Bref, je suis injoignable.</span>

<!--more-->


Comme tout le monde, je suis extrêmement sollicitée : appel, SMS, message, mail, notification…
Il peut être difficile de **détecter les messages importants**, pour soi.
De plus, la **batterie du téléphone** est mise à rude épreuve.

<span Style="font-variant-caps:small-caps">« Je ne crois pas qu’il y ait de bonne ou de mauvaise » façon de gérer ses communications.</span>

Dans cet article, je dévoile mes règles afin de ne pas me laisser distraire et de m’occuper
des missions de la plus haute importance, tout en gérant les urgences.
En particulier, ces règles de contact s'axent autour :
+ des outils de communication / publipostage (téléphone, mail et réseaux sociaux)
+ des listes de contact (_yellow list_)
+ des espaces temps pour répondre

## Jamais sans mon _iPhone_
Ce téléphone principal me permet de gérer les **urgences**. Afin de préserver sa batterie,
seulement les applications de base sont installés (_Maps_ et basta).
Il m'arrive de répondre aux **appels** si le numéro est dans la _yellow list_
et si je ne suis pas en train de :
* méditer à la plage
* donner un cours ou une formation (except _yellow list_)
* travailler en mode déconnecté (except _yellow list_)

Par ailleurs, les **appels professionnels** sont généralement organisés
en rendez-vous pris au préalable, par SMS, mail, ou message sur un des réseaux sociaux.

Dans tous les cas, un message sur le **répondeur** me permet :
* d'évaluer l'urgence ou l'importance de l'appel
* ne pas bloquer le numéro (s'il n'est pas dans une des listes de contact)

Par ailleurs, les **SMS** sont bienvenus, cela dit j'y réponds si c'est important.

Ce téléphone principal est dédié aux communications via le **réseau cellulaire**.
Un téléphone secondaire me permet de traiter les communications via les réseaux sociaux.

## J'aime lire sur mon _Android_
Mon téléphone de travail est un _Android_, dont la taille d'écran fait environ 6 pouces. Il contient les principales applications de **messageries** et de **réseaux sociaux**.

D'ailleurs, voici mes réseaux préférés :
  + _LinkedIn_ [Macha DA  COSTA](https://www.linkedin.com/in/machadacosta/)
  + _Facebook_ [ChillCoding](https://www.facebook.com/chillcoding/)
  + _Instagram_ [@chillcoding](https://www.instagram.com/chillcoding/)
  + _Twitter_ [@machadacosta](https://twitter.com/machaDaCosta)

Il est **connecté en Wifi** ou sur partage de connexion lors de villégiature, de **temps en temps**, lors
d'**espaces limités dans le temps**.

## J'espace mes temps de communication
Habituellement, le **matin** je traite mes **mails** et les messages _LinkedIn_
et l'après-midi je développe en mode déconnecté : téléphone secondaire en **mode avion**
et téléphone principal en **mode silencieux**.

D'une part, sur le réseau cellulaire, je suis relativement réactive.

D'autres part, sur les réseaux sociaux, c'est plutôt aléatoire. En effet, selon l'importance, l'intérêt,
je mets 1 minute, 1 heure, 1 jour, 1 semaine, voir 1 an à répondre
(jamais quand c'est pas important pour moi ou "intrusif").



<span Style="font-variant-caps:small-caps">Finalement, pour gérer ses communications, deux téléphones c'est confortable.</span>
Cela permet de **répartir** l'utilisation des outils de communication.
Aussi ça permet d'avoir une **batterie illimitée**. Ils peuvent tenir jusqu'à 5 jours en autonomie : lorsque le principal tombe en rade, il y a toujours une deuxième chance, en changeant la carte SIM de téléphone.
<span Style="font-variant-caps:small-caps">Et vous ? Comment faites-vous pour gérer ce chahut de messages et d'appels ?</span>

Si vous souhaitez partager sur votre façon d'utiliser les outils de communication, n'hésitez pas à commenter cet article ou discuter sur l'un des réseaux sociaux !

### {% icon fa-angellist %} Références
1. [Pierre-Emmanuel Barré : Jour #4, 17 mars 2020](https://www.youtube.com/watch?v=nwbYmcsuio0){:target="_blank"}


Merci aux relecteur·rice·s pour leur participation à l'élaboration de cet article.
