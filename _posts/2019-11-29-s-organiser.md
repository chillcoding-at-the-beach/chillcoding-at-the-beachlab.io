---
title: "Astuces pour s'organiser"
categories: fr chillcoding
author: macha
last_update: 2020-30-04
---

« Ce matin mon réveil a sonné à 7:30, j'avais pas envie de me lever alors je l'ai éteint.

Finalement, je me suis levée à 08:08.

Après le petit dej, je me suis dis : c’est bizarre, je me fais pas emmerder aujourd’hui.

J’ai enlevé le mode avion, j’avais 6 appels en absence, 13 messages et 38 notifications. »

Bref, bienvenue en 2020.

<!--more-->
Toutes ces publicités, ces notifications, etc. auxquelles nous sommes exposés,
nous détourne de ce qui est important et prioritaire.

Cet article expose 8 astuces pour s'organiser au quotidien,
je les ai mises en place, au fil de l'eau, après être devenue indépendante,
en juin 2014.

Cela dit, chacun est différent et doit trouver ses propres techniques d'organisation.

## Mise en place d'une routine
**Le matin**, avant de commencer à travailler, et d'être happée par l'ordinateur,
je réalise une liste de <u>tâches journalière</u>.

Aussi, je réalise une _todo_ personnelle, pour me vider la tête
et je fixe des horaires pour ne pas être trop à l'arrache.

**Entre midi et deux**, je fais du yoga histoire de décompresser.


**Le soir**, je fête la fin de la journée et m'attèle à ma _todo_ perso.

## Mise en place d'une liste de tâches

Une liste de tâches peut vite devenir surchargée.
Afin de s'y retrouver, j'utilise 2 techniques.

La première consiste à établir un ordre, les tâches **urgentes** portent le numéro 0.
Ensuite, les tâches **importantes** portent les numéros suivants selon leur niveau d'importance, d'intérêt.

La seconde technique consiste à définir une taxonomie de tâches :
* Administratif
* Développement
* Communication
* Sport
* Casa
* Autres

Chacune de ces catégories a une icône, cela permet de rapidement se repérer
par rapport à un espace temps.

## Mise en place d'espace temps
Le principe consiste à allouer un créneau pour une catégorie de tâches.
Il s'agit de réaliser une à plusieurs tâche·s dans un temps limité.

Par exemple,
**le matin** :
- Réalisation de tâches liées à la communication (Mail, LinkedIn, Rdv, Todo Freestyle, etc.)

**l'après-midi** :
- Réalisation de tâches liées au coeur de mon travail (le développement d'application mobile)

**En mode déconnecté** afin de me concentrer à fond !

Par ailleurs, j'ai également mis en place une **routine hebdomadaire** :
le lundi soir c'est séance de musique, le mardi soir c'est sport, etc.

En général, je fais cela mais ça peut changer en fonction des urgences,
de l'importance des tâches et des priorités du moment.

## Mise en place d'objets
L'idée est d'utiliser des objets afin de se rappeler des tâches à réaliser.
Il s'agit de les placer de manière judicieuse. Ici, l'objet sert de **rappel**.

Par exemple, laisser un vêtement blanc dans le lavabo, pour le nettoyer assidument
quand j'en aurai le temps.

## Minimalisme
Le minimalisme consiste a posséder seulement l'essentiel
(cf. livre « goodbye, things » de Fumio Sasaki).
Il s'agit de pratiquer cette philosophie aussi bien **dans le réel** que **dans le digital**.

## Le Lundi au Soleil
Ce jour là est toujours spécial, c'est l'occasion de mettre en place
la <u>liste hebdomadaire</u> et de réajuster la routine d'espace temps si besoin.

## Le Jeudi on fait le Bilan
À la fin de la journée, il s'agit de **barrer toutes les tâches réalisées**,
cela créer un sentiment d'accomplissement, un certain bien-être au travail.

« Il y a seulement 24 heures dans une journée. »

Les tâches inachevées iront sur la liste de la semaine prochaine.

## Le Vendredi à la Plage
Chez _ChillCoding_, nous n'avons pas attendu l'étude de Microsoft [\[1\]](#temps)
pour travailler un jour de moins.
En effet, nous pressentions un gain d'efficacité en ne travaillant que 4 jours.

Bref, je suis complètement à l'arrache.

N'hésitez pas à partager vos astuces en commentaire ou à me poser des questions si vous souhaitez approfondir certains points.

### {% icon fa-angellist %} Références

1. <a name="temps"></a>[Les Echos : Au Japon, Microsoft teste la formule travailler moins pour produire plus, 15 novembre 2019](https://www.lesechos.fr/monde/asie-pacifique/au-japon-microsoft-teste-la-formule-travailler-moins-pour-produire-plus-1145464){:target="_blank"}

2. [Fabulous](https://play.google.com/store/apps/details?id=co.thefabulous.app){:target="_blank"}

3. Formation "Optimiser son temps et ses priorités"

4. [Ooreka : Gestion du temps](https://carriere.ooreka.fr/comprendre/gestion-du-temps)
