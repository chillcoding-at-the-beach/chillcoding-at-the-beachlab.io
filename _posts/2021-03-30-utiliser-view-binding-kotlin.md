---
title: "Accéder facilement aux éléments d'une vue Android [AK 4 UI Native]"
categories: fr coding tutoriel android kotlin
author: macha
last_update: 2023-01-16
---

<div class="text-center lead" markdown="1">
  <img src="/app/kotlin-for-android/images/gallery/thumbs/4_android-ui.svg"
  alt="Interface Utilisateur Native dans Kotlin pour Android" width="200px"/>
</div>

Ce tutoriel détaille comment utiliser le _View Binding_, de _Jetpack_, dans un projet _Android Studio_. Cela afin d'accéder facilement aux éléments de la vue, déclarées dans un fichier _XML_.

Plus précisément, il est indiqué comment configurer le projet _Android_ et comment utiliser le _View Binding_ depuis
une <b style='color:green'>Activity</b>, un <b style='color:green'>Fragment</b>, ou un <b style='color:green'>RecyclerView</b>.

<!--more-->

Dans cet article, il est expliqué comment :
  * [Configurer View Binding]({{ page.url }}#configurer)
  * [Utiliser View Binding depuis une Activity]({{ page.url }}#activity)
  * [Utiliser View Binding depuis un Fragment]({{ page.url }}#fragment)
  * [Utiliser View Binding avec un RecyclerView]({{ page.url }}#recyclerview)


## Pré-requis : Savoir créer un projet Android

1. Créez un projet _Android Studio_, en cochant le support du langage _Kotlin_
2. Choisissez  comme modèle d'activité la vide soit `Empty Activity`
3. Laissez les noms proposés par défaut **MainActivity** pour l'<b style='color:green'>Activity</b> et **activity_main.xml** pour la vue (ou <i style='color:green'>layout</i>)
4. Dans **MainActivity**, remarquez l'appel de la fonction <i style='color:green'>setContentView()</i> permettant de lier la classe et la vue, dans la méthode <i style='color:green'>onCreate()</i>

    ```kotlin
      ...
      override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
      }
      ...
    ```

Remarque : La fonction <i style='color:green'>setContentView()</i> prend en paramètre la vue créée par défaut **activity_main.xml**. Dans le code (_Kotlin_ ou _Java_), on accède à cette vue via le fichier `R`, ce dernier contient toutes les références des ressources et il est automatiquement généré par _Android Studio_. Ainsi une vue contenue dans le dossier `layout/` est accessible dans le code via `R.layout.` + le nom du fichier _XML_ qui la contient.

## Configurer View Binding <a name="configurer" />

1. Placez vous dans le fichier _gradle_ relatif au module du projet, soit
`build.gradle (Module: app)`

2. Dans la partie android, autorisez l'option `viewBinding` :

    ```xml
        android {
          ...
          buildFeatures {
            viewBinding true
          }
    ```

Remarque : _AS_ configure par défaut un projet _Android_ avec les 3 plugins suivants :

 * `com.android.application` : contient les classes relatives au _SDK Android_
 * `kotlin-android` : pour le support du langage _Kotlin_


##  Accéder à un élément de la vue

## Utiliser le _View Binding_ depuis une _Activity_ <a name="activity" />

Si vous exécutez le projet créé par défaut, avec le modèle d'activité vide, alors
vous devriez voir à l'écran le texte _Hello World!_ s'afficher.

Dans cette partie, l'objectif est de modifier ce texte par _Hello Kotliners :)_ ,
via la classe **MainActivity**, en utilisant le _View Binding_ depuis le code
d'une <b style='color:green'>Activity</b> _Kotlin_.

1. Placez vous dans la vue, soit le fichier **activity_main.xml** situé dans le dossier `layout/`

2. Ajoutez un identifiant, par exemple _mainText_, à l'élément graphique de type
<b style='color:green'>TextView</b>
 * soit via l'éditeur graphique intégré à _AS_
 * soit directement dans le fichier _XML_, en passant en mode `Text` plutôt que `Design`

    ```kotlin
    <LinearLayout ... >
      <TextView android:id="@+id/mainText" />
    </LinearLayout>
    ```

   Remarque : l'annotation`@+id/` pour la déclaration d'un nouvel identifiant _XML_.

3. Dans la classe principale de votre projet : **MainActivity**, déclarez la variable `binding`

    ```kotlin
      private lateinit var binding: ActivityMainBinding
    ```

   Note : _ViewBinding_ génère une classe portant le nom du fichier XML en Pascal avec le mot "Binding" accolé,
   cela pour chaque fichier de vue (sauf s'il comporte `tools:viewBindingIgnore="true"`).

4. Dans la méthode <i style='color:green'>onCreate()</i>, créez une instance de `ActivityMainBinding` :

    ```kotlin
      ...
      override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
      }
      ...
    ```

5. Dans la méthode <i style='color:green'>onCreate()</i>, passez la vue racine à <i style='color:green'>setContentView()</i>

    ```kotlin
      ...
      override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
      }
      ...
    ```

   Note : À présent, il est possible d'accéder aux éléments graphiques via la variable `binding`.

6. Dans le code _Kotlin_, modifiez le texte, via l'attribut <i style='color:green'>text</i> de la variable _mainText_,
(disponible pour tous les objets de type <b style='color:green'>TextView</b>) :

       binding.mainText.text = "Hello Kotlin :)"


Enfin, après exécution du projet, vous devriez voir s'afficher à l'écran _Hello Kotlin :)_ !

Envie de tester le sélecteur de date sur _Android_ ? Retrouvez le code dans le thème
"Interface Utilisateur Native" de l'app. "Kotlin pour Android : quiz".
<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak4-datepicker.png" alt="sélecteur de date Android kotlin"/>
</div>
{% include aside.html %}

## Utiliser le _View Binding_ depuis un _Fragment_ <a name="fragment" />

Une alternative est d'utiliser le  _View Binding_ depuis un <b style='color:green'>Fragment</b> _Kotlin_.

Dans cette partie, l'objectif est de modifier un texte depuis une classe **MainFragment**.

1. Placez vous dans la vue, soit le fichier **fragment_main.xml** situé dans le dossier `layout/`

2. Ajoutez un identifiant, par exemple _mainText_, à l'élément graphique de type
<b style='color:green'>TextView</b> :

    ```kotlin
    <LinearLayout ... >
      <Button android:id="@+id/mainButton" />
    </LinearLayout>
    ```

3. Dans la classe **MainFragment**, déclarez la variable `binding`

    ```kotlin
      private var _binding: FragmentMainBinding? = null
      // This property is only valid between onCreateView and
      // onDestroyView.
      private val binding get() = _binding!!
    ```

4. Dans la méthode <i style='color:green'>onCreateView()</i>, créez une instance de `FragmentMainBinding` et retournez la vue racine :

    ```kotlin
      ...
      override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment (false for attach to the root)
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
      }
      ...
    ```

4. Dans la méthode <i style='color:green'>onDestroyView()</i>, libérez l'instance de `FragmentMainBinding` :

```kotlin
  ...
  override fun onDestroyView() {
      super.onDestroyView()
      _binding = null
  }
  ...
```

5. Dans la méthode <i style='color:green'>onViewCreated()</i>, gerez le clique sur le bouton, via la <i>lambda</i> <i style='color:green'>setOnClickListener</i> de la variable _mainButton_,

    ```kotlin
      ...
      override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mainButton.setOnClickListener { Toast.makeText(requireContext(), "Be Happy", Toast.LENGTH_LONG).show() }
      }
      ...
    ```

## Utiliser _View Binding_ avec un _RecyclerView_

Une optimisation est d'utiliser le  _View Binding_ depuis le code d'un <b style='color:green'>Adapter</b> _Kotlin_,
lié à un <b style='color:green'>RecyclerView</b>.

Par exemple, pour l'affichage de la liste des versions Android (cf. [AK-6]).

1. Le fichier de vue **item_and_version.xml**, situé dans le dossier `layout/`, contient un <b style='color:green'>TextView</b> :

    ```kotlin
     <TextView
         android:id="@+id/andVersionTxt"
         android:layout_width="match_parent"
         android:layout_height="@dimen/common_size" />

    ```

2. L'<b style='color:green'>Adapter</b> initialise la variable `binding` depuis la fonction <i style='color:green'>onCreateViewHolder()</i> :

    ```kotlin
     val binding = ItemAndVersionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
     return ViewHolder(binding)
    ```

3. La déclaration du <b style='color:green'>ViewHolder</b> de l'<b style='color:green'>Adapter</b> devient alors :

    ```kotlin
     inner class ViewHolder(val binding: ItemAndVersionBinding) : RecyclerView.ViewHolder(binding.root)
    ```

4. Les éléments graphiques sont alors accessibles via la variable `holder.binding` ou `binding` :

    ```kotlin
     bindind.andVersionTxt.text = "$name"
    ```

##  Comparer avec la solution habituelle

Sans le _View Binding_, afin de modifier un élément de la vue depuis du code :
d'une part, il s'agit d'ajouter un identifiant à l'élément graphique, comme fait
précédemment; d'autre part, il s'agit de récupérer cet élément via la fonction
<i style='color:green'>findViewById()</i> prenant en paramètre sa référence `R.id.mainText`.
Il s'agirait donc de coder la ligne supplémentaire :

* en _Kotlin_

```Kotlin
var mainText = findViewById<TextView>(R.id.mainText) as TextView
```
ou bien

```kotlin
var mainText:TextView = findViewById(R.id.mainText)
```

Remarquez la conversion, en <b style='color:green'>TextView</b>, de l'objet
renvoyé par la fonction <i style='color:green'>findViewById()</i>. De plus, une
déclaration de variable est faite : _mainText_, cela est sujet à des **fuites mémoires**;
en utilisant le _View Binding_, cela vous assure une bonne gestion des
éléments de la vue. En effet, ils sont mis en cache
dans des variables déclarées une seule fois.

Par ailleurs, les appels à la fonction <i style='color:green'>findViewById()</i> sont très couteux.
Utiliser le _View Binding_ avec un <b style='color:green'>RecyclerView</b> assure une optimisation
de l'affichage d'une liste d'éléments.

Remarque : L'intérêt de modifier les éléments graphiques depuis du code, plutôt que
directement dans le fichier _XML_, c'est de pouvoir les modifier dynamiquement
pendant l'exécution de l'application. Par exemple, un texte pourrait être changé
si l'utilisateur clique sur un bouton.

Finalement, cet article explique comment le _View Binding_ est ajouté dans un
projet _Kotlin Android_. Ensuite, il détaille comment accéder aux éléments de la
vue depuis du code _Kotlin_, via une variable _binding_, cela depuis une <b style='color:green'>Activity</b>,
un <b style='color:green'>Fragment</b>, ou un <b style='color:green'>Adapter</b>.


### {% icon fa-globe %} Référence

* [Android Developers: View Binding, Part of Android Jetpack](https://developer.android.com/topic/libraries/view-binding)

*[AS]: Android Studio
[AK-6]: https://www.chillcoding.com/blog/2018/10/22/creer-liste-recyclerview-kotlin-android/
