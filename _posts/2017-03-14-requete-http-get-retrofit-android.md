---
title: "Faire une requête HTTP GET avec Retrofit en Kotlin Android [AK 9 HTTPS]"
categories: fr coding tutoriel android-serveur kotlin
author: macha
last_update: 2018-12-05
---

<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/gallery/thumbs/9_android-network.svg"
  alt="Communication HTTPS dans l'app. Kotlin pour Android" width="200px"/>
</div>

Ce tutoriel va te donner les clés pour faire une requête **HTTPS GET** au format **JSON**,
en _Android_, avec la bibliothèque [_Retrofit V2_][Retrofit],
sous le langage _Kotlin_.

<!--more-->

Concrètement, nous allons demander une liste de pays à un serveur web,
[REST Countries APIs][Server], en suivant les étapes suivantes :

- Importer _Retrofit_ et _Moshi_ dans le projet Android
- Créer la classe _Kotlin_ représentant les données (à récupérer du serveur)
- Créer l'interface _Kotlin_ représentant l'API du serveur
- Créer l'instance du client _Retrofit_ (en version 2)
- Créer l'instance du service d'API
- Créer la requête **GET**
- Exécuter la requête **GET**
- Récupérer le résultat de la requête

## Importer Retrofit et Moshi dans le projet Android

Tout d’abord, il faut au préalable importer, dans le projet _Android_, la
dépendance [_Retrofit 2_][Retrofit], ainsi qu'un convertisseur de requête :

        implementation "com.squareup.retrofit2:retrofit:$retrofit_version"
        implementation "com.squareup.retrofit2:converter-moshi:$moshi_version"

avec :

            ext.retrofit_version = '2.9.0'
            ext.moshi_version = '2.9.0'

Il s'agit aussi d'ajouter la permission internet, dans le fichier _Manifest_ :
```xml
<uses-permission android:name="android.permission.INTERNET" />
```

Note : la bibliothèque [_OkHttp_][Okhttp] est utilisé pour écrire et lire les _sockets_
par [_Retrofit V2_][Retrofit].

Pour plus de détail, consultez l'article [Configurer un projet Android pour utiliser Retrofit][Configure].

Dans la suite, nous nous intéressons à réaliser une requête **HTTPS GET** et à
récupérer une liste d'objets au format **JSON** depuis un serveur distant.

## Créer la classe Kotlin représentant les données

Il s'agit de créer la classe _Kotlin_ représentant l'objet à récupérer du serveur.

Par exemple, nous souhaitons récupérer une liste des pays, depuis le serveur [Countries APIs][Server].
La requête permettant d'obtenir la liste de pays est _<https://restcountries.com/v3.1/all>._ 
En utilisant [Postman][Postman], il est possible d'analyser le résultat **JSON** de cette requête :

```json
[
{
    "name": {
        "common": "Barbados",
        "official": "Barbados",
        "nativeName": {
            "eng": {
                "official": "Barbados",
                "common": "Barbados"
            }
        }
    },
    "tld": [
        ".bb"
    ],
    "cca2": "BB",
    "ccn3": "052",
    "cca3": "BRB",
    "cioc": "BAR",
    "independent": true,
    "status": "officially-assigned",
    "unMember": true,
    "currencies": {
        "BBD": {
            "name": "Barbadian dollar",
            "symbol": "$"
        }
    },
    "idd": {
        "root": "+1",
        "suffixes": [
            "246"
        ]
    },
    "capital": [
        "Bridgetown"
    ],
    "altSpellings": [
        "BB"
    ],
    "region": "Americas",
    "subregion": "Caribbean",
    "languages": {
        "eng": "English"
    },
    "translations": {
        "ara": {
            "official": "باربادوس",
            "common": "باربادوس"
        },
        "bre": {
            "official": "Barbados",
            "common": "Barbados"
        },
        "ces": {}
      }
    }
  ]
```

Ainsi le serveur, renvoie une liste de pays, contenant :

   - un objet (_name_)
   lui même contenant
   - un nom (_common_)

Afin de modéliser un tel objet, soit _Country_, il faut créer les classes _Kotlin_
correspondant à la réponse du serveur. Par exemple, la classe `Country` représente un pays et
`CountryDetail` les détails du pays, selon les données du serveur :

```kotlin
data class Country(val name: CountryDetail)
data class CountryDetail(val common: String)
```

Les attributs du constructeur doivent porter les mêmes noms que ceux du fichier **JSON**
renvoyé par le serveur : _name_ et _common_. Pour le moment, seulement ces deux attributs
nous intéresse, c'est pourquoi les autres n'apparaissent pas dans la classe.

## Créer l'interface Kotlin représentant l'API

Il s'agit de créer l'interface _Kotlin_ contenant la déclaration des
requêtes disponibles sur [Countries APIs][Server].
Pour le moment, nous avons une seule requête **GET**, ne prenant aucun paramètre
et renvoyant une liste de pays.
Par exemple, l'interface `CountriesService` représente l'API du serveur de cours :

```kotlin
interface CountriesService {
    @GET("/v3.1/all")
    fun listCountries(): Call<List<Country>>
}
```

Avec les importations suivants :

    import retrofit2.Call
    import retrofit2.http.GET

L'annotation `@GET` de [Retrofit 2][Retrofit] indique la déclaration d'une requête **GET**.
Ensuite, en paramètre, il est placé le chemin de la requête `/v3.1/all`.

La signature de la fonction associé à la requête porte le nom `listCountries`
(son nom n'a pas d'importance). Elle ne prend pas de paramètre puisque la requête n'en demande pas.
Et elle renvoie un `Call<...>`, c'est la réponse du serveur, prenant entre chevron
`List<Country>`, c'est-à-dire la liste de pays renvoyée par le serveur au format **JSON**.

Toutes fonctions associées à une requête, retourne un objet `Call<>`, ensuite
le paramètre entre chevron varie selon ce que renvoie le serveur (une liste ou un objet)
et ce que l'on souhaite récupérer. Par ailleurs, si la requête requiert un paramètre
tel qu'un nom d'utilisateur alors il est spécifié en paramètre de la fonction.

Par exemple pour récupérer la liste des répertoires d'un utilisateur **GitHub**,
selon leur API, il faudrait coder en _Kotlin_ :

    @GET("/users/{user}/repos")
    listRepos(@Path("user") user: String): Call<List<Repo>>

## Créer l'instance du client Retrofit

Depuis, une <i style='color:green'>Activity</i> ou bien un
<i style='color:green'>Fragment</i>, nous allons créer une instance de client _Retrofit_.
Par exemple, dans un <i style='color:green'>Fragment</i> `NetworkFragment`:

        val retrofit = Retrofit.Builder()
         .baseUrl(URL_COUNTRY_API)
         .addConverterFactory(MoshiConverterFactory.create())
         .build()

avec l'url :

```kotlin
companion object {
    const val URL_COUNTRY_API = "https://restcountries.com/"
}
```

les imports concernés sont celui de [_Retrofit 2_][Retrofit]  et de [_Moshi_][Moshi], le convertisseur de **JSON**  :

        import retrofit2.Retrofit
        import retrofit2.converter.moshi.MoshiConverterFactory

Le client _Retrofit_ est à configurer avec une url de serveur ainsi qu'un
convertisseur de requête. Ici, il est choisi le convertisseur _Moshi_ car il permet
de convertir du **JSON** en objet _Kotlin_.

## Créer l'instance du service

Après avoir créé l'instance du client _Retrofit_, à la suite dans `NetworkFragment`, il s'agit de créer
l'instance du service comme suit :

        val service = retro.create(CountriesService::class.java)

et d'importer la classe `CountriesService`.

Ce service est créé à partir du client _Retrofit_, lequel contient l'url du serveur,
et à partir du `.class` de l'interface, laquelle contient toutes les requêtes possibles
avec le serveur.

## Créer la requête GET

Toujours à la suite dans `NetworkFragment`, il s'agit de créer la requête **GET** :

        val countryRequest = service.listCountries()

## Exécuter la requête GET

Enfin, il est possible d'exécuter la requête venant d'être créée via la fonction
<i style='color:#00bfff'>enqueue(object: Callback<...>)</i> de _Retrofit_ :

```kotlin
countryRequest.enqueue(object : Callback<List<Country>> {
        override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
            val allCountry = response.body()
            for (c in allCountry!!)
                Log.v(
                    MainActivity::class.simpleName,
                    "NAME: ${c.name.common} \n "
                )
        }


        override fun onFailure(call: Call<List<Country>>, t: Throwable) {
            Log.i(MainActivity::class.simpleName, "RETRY AND GOOD LUCK.")
        }

    })
```

les importations associées sont les suivantes :

          import retrofit2.Call
          import retrofit2.Callback
          import retrofit2.Response

ainsi que l'import de la classe `Country`.

La fonction <i style='color:#00bfff'>enqueue(object: Callback<...>)</i> prend en
paramètre l'objet <i style='color:#00bfff'>Callback</i>,
cet objet intercepte la réponse du serveur.
Cette dernière est traitée dans la sur-implémentation  des fonctions `onResponse(...)`
et `onFailure(...)`.
Les paramètres du <i style='color:#00bfff'>Callback</i> ainsi que des fonctions
`onResponse(...)` et `onFailure(...)` dépendent directement du type de retour
de la fonction représentant la requête. Dans notre cas, le type de retour de
`listCountries()` est `List<Country>` (définit par ce que renvoie le serveur).

## Récupérer le résultat de la requête

Enfin, la liste des pays en contenu dans `response.body()`.

Le code correspondant peut être téléchargé dans le thème "Communication HTTPS" de l'application
"Kotlin pour Android : Quiz".

<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak9.png" alt="Communication HTTPS avec API de pays"/>
</div>

{% include aside.html %}


Finalement, dans cet article nous avons vu comment :

- Importer _Retrofit_ et _Moshi_ dans le projet Android
- Créer la classe _Kotlin_ représentant les données à récupérer du seveur
- Créer l'interface _Kotlin_ représentant l'API du serveur
- Créer l'instance du client _Retrofit_ en version 2
- Créer l'instance du service d'API
- Créer la requête **GET**
- Exécuter la requête **GET**
- Récupérer le fruit de la requête



### {% icon fa-globe %} Références
1. [Retrofit]( http://square.github.io/retrofit/)
2. [Okhttp](http://square.github.io/okhttp/)
3. [Moshi](https://github.com/square/retrofit/tree/master/retrofit-converters/moshi)
4. [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop)

[Retrofit]: http://square.github.io/retrofit/
[Okhttp]: http://square.github.io/okhttp/
[Moshi]: https://github.com/square/retrofit/tree/master/retrofit-converters/moshi
[Postman]: https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop
[Server]: http://restcountries.com/
[Configure]: https://www.chillcoding.com/blog/2015/11/16/configurer-projet-android-pour-retrofit/
