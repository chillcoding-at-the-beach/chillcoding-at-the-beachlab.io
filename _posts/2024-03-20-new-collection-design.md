---
title: "New Collection DESIGN Printemps-Été [DESIGN]"
categories: fr design tendance
author: macha
permalink: /new-collection-design/
last_update: 2024-03-20
---


<div class="text-center lead" markdown="1" >
  <img src="/assets/img/post/design.png"
  alt="lady design" width="200px"/>
</div>

«
— Quelles sont les tendances Design 2024 ?

> _Qu'est-ce qu'elle a dit ? Je suis pas réveillé..._

— Quelles sont les tendances 2024 ? Vous avez 1 semaine !

> _Madame, c'est à faire en groupe ?_

— Oui.

> _Facile ! On va demander à CHAT GPT!_

— Attention, j'attends un PDF qui sort de l'ordinaire, soyez imaginatif !

> _Quoi?!_

— ^- Bref, on a cherché "Tendance 2024" sur Google...»

<!--more-->

Les tendances 2024 sont nombreuses, elles reprennent celles des années précédentes,
toujours au goût du jour. Aussi les tendances émergentes sont mises sur le devant
de la scène.

Dans cet article, sur la "nouvelle Collection DESIGN Printemps-Été", elles sont
regroupées en 5 catégories :
<div class="text-center lead" markdown="1" >
  <img src="/assets/img/post/tendance-2024-graphique.png" width="" alt="Graphique circulaire des tendances 2024"/>
</div>

1. IA
2. Mode sombre
3. Accessibilité
4. Design Vocal, 3D, etc.
5. MINIMALISM & Simplicity


## 1. L’IA au service du Design

« Une image vaut mille mots » :  image, icône, graphique sont essentiels en _Design_.
Une image permet de faire passer des idées, sans avoir à écrire des tartines,
ou à lire des § (§=paragraphe). Cela écrit, il est toujours intéressant d’avoir
des explications (voir des descriptions textuelles) afin de s’assurer
d’une compréhension commune.

Aujourd’hui, les banques d’images sont peu à peu remplacées par l’IA. En effet,
certains sites comme <a href="https://app.leonardo.ai/">app.leonardo.ai</a> ou
<a href="https://picsart.com/">picsart.com</a> proposent des images, libre de droit,
à partir de mots clés, ou d’une description de ce que l’on souhaite.
Aussi l’outils [canva.com](canva.com), permettant de créer des documents, intègre
directement des IA pour générer image et texte.

Bref. en 2024 l’IA est partout, elle envahie nos outils préférés pour augmenter
notre efficacité.

<u>Note </u>: L’IA c’est comme un copilote, ma préférée : [chat.mistral.ai](chat.mistral.ai) (made in 🇫🇷).

Il faut juste lui poser les bonnes questions !
D'ailleurs, tu en trouveras quelques unes dans les ateliers de la formation _UI / UX Design_ :
{% include aside-design.html %}

## 2. Mode sombre ou _Dark Mode_
Ce n’est pas nouveau, cela fait plusieurs années : beaucoup de personne préfère
le _Dark Mode_. Aussi, on tend à l’**Hyper personnalisation** c’est-à dire
la personnalisation du thème.

## 3. Accessibilité renforcée
De manière générale, l'accessibilité vise à garantir l'égalité des chances et
à permettre à toutes les personnes (quels que soient leurs besoins) de participer
pleinement à la vie sociale et économique. Ici, renforcé signifie que le niveau
d'accessibilité est supérieur à celui exigé par la législation en vigueur.

## 4. Design Vocal, 3D, etc.
Il y a une tendance à l’**interface conversationnelle** couplée avec des interactions
sans écran (navigation par geste). En outre, le Design 3D est de plus en plus plébiscité.
Par ailleurs, le **Design éco-Responsable**, ou Éco-design n’est pas seulement une mode,
c’est un mouvement ! Attention au micro-interactions et animation. La suite de ce §,
concerne le etc. soit et cetera : la palette de couleur se veut sobre avec une
<span class="brand-o"><i>french touch</i></span>, sans mentionner la TyPoGraphie
(cf. <i>UX / UI Design by</i> Macha da Costa).

## 5. MINIMALISM & Simplicity
« Trop d’information tue l’information » : la **visualisation de données** devient
un enjeu majeur. Premièrement, il est important de suivre quelques unes des 12 règles
d’ergonomie afin de rendre l’information accessible
(cf. [Formation _UI / UX Design_](https://formation.chillcoding.com/courses/ux-design)).

Deuxièmement, la grille Bento ou _Bento Grid_ est une organisation graphique
émergente en _Web Design_:
<div class="text-center lead" markdown="1" >
  <img src="/assets/img/post/bento-grid.png" alt="Graphique circulaire des tendances 2024"/>
</div>

Finalement, les tendances 2024 sont pas nouvelles quand on suit le _flow_.

### {% icon fa-globe %} Références :

1. [chat.mistral.ai](chat.mistral.ai)
2. [CHILLcoding.com: Formation UI / UX Design](https://formation.chillcoding.com/courses/ux-design)
3. [arquen: Tendances de l’UX Design en 2024](https://www.arquen.fr/blog/tendances-de-lux-design-en-2024-vers-un-futur-plus-immersif-et-personnalise/)
