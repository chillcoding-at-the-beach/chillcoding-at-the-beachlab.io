---
title: "Faire le tour du tableau liste en Kotlin [AK 2 Kotlin]"
categories: fr coding tutoriel android kotlin
author: macha
last_update: 2023-04-07
---

<div class="text-center lead" markdown="1">
  <img src="/app/kotlin-for-android/images/gallery/thumbs/2_kotlin.svg"
  alt="Kotlin dans l'app. Kotlin pour Android" width="200px"/>
</div>

Cet article présente les principales façons d'initialiser et d'utiliser les collections,
tableau, liste, avec le langage _Kotlin_.

<!--more-->

Avec _Kotlin_ rien n'est nul ou presque, une collection devra donc être initialisé
dès le départ, selon le contexte :
+ Que va contenir la collection ? type primitif ou
objet ;
+ Quelle sera la taille de la collection ? fixe, maximum, dynamique (c'est-à-dire
connue à l'exécution).

Afin d'optimiser les performances du programme, l'initialisation
d'une collection demande une attention particulière.

Enfin, la manipulation d'un tableau ou d'une liste reste similaire
aux autres langages de programmation.


## Initialisation avec des valeurs

Lorsque les valeurs sont connues à l'avance, l'initialisation du tableau est optimale
d'un point de vue des performances.

```kotlin
val nameArray = arrayOf("Eclair", "Lolipop", "Nougat")
```
Dans l'exemple précédent, il est créé un tableau contenant trois chaînes de caractères.
Le type implicite de ce tableau est `Array<String>`.

```kotlin
val imgArray = arrayOf(R.drawable.eclair, R.drawable.lollipop, R.drawable.nougat)
```
Ici, `imgArray` est un tableau d'identifiants d'images, présentes dans les ressources d'un projet _Android_.
Le type implicite de ce tableau est `Array<Int>`, soit `Integer[]` en _Java_.

Dans ce cas, les éléments du tableau sont de types primitifs,
il est alors intéressant d'utiliser un `IntArray` :

```kotlin
val imgArray = intArrayOf(R.drawable.eclair, R.drawable.lollipop, R.drawable.nougat)
```
Note : il sera traduit par un tableau de type `int[]` en _bytecode_ _Java_
(plus léger).

## Initialisation avec une taille fixe

Dans le cas où l'on ne connait pas la taille du tableau à l'avance,
il peut être avantageux de fixer une taille maximum. En particulier, lorsque les
éléments sont de types primitifs.
Cela permet d'optimiser les performances du programme
(le compilateur sait combien d'espace allouer à notre tableau).

Lorsque la taille du tableau est fixe, il s'agit d'utiliser le constructeur
de `Array<>`, il prend :
 * entre chevrons, le type des éléments du tableau ;
 * en premier paramètre, la taille du tableau ;
 * en second paramètre, une fonction _lambda_, elle initialise le tableau à partir
de l'index `i`.

```kotlin
val magicArray = Array<MagicCircle>(7, { i: Int -> MagicCircle(i, i) })
```
Dans cet exemple, `magicArray` est un tableau contenant sept cercles magiques.
_MagicCircle_ est un type créé afin d'illustrer des développements classiques
de Programmation Orientée Objet (POO).
Il possède deux coordonnées : une abscisse `cx` et une ordonnée `cy`.
Voici la classe qui le définit :

```kotlin
data class MagicCircle(var cx: Int, var cy: Int)
```

## Initialisation avec une taille inconnue

Dans le cas où la taille du tableau est inconnue,
il est préférable d'utiliser une liste, comme en _Java_ [\[2\]](#list).
Plus spécifiquement, il s'agit d'utiliser une liste mutable
(par défaut une liste n'est pas mutable, contrairement à un tableau [\[3\]](#array)).

```kotlin
val mutableList = mutableListOf<MagicCircle>()
```
Pour les expert·e·s en collection,
il est possible d'utiliser un `ArrayList`.

```kotlin
val arrayList = ArrayList<MagicCircle>()
```
Dans le cas où la liste est obtenue plus tard (après une requête réseau
ou base de données), il est intéressant de l'initialiser avec `emptyList` :

```kotlin
var emptyList = emptyList<MagicCircle>()
```
Elle peut alors être initialisée plus tard :

```kotlin
emptyList = listOf(MagicCircle(24, 67), MagicCircle(24, 56))
```

## Parcours d'un tableau ou d'une liste

Concernant le parcours, il s'agit d'utiliser une boucle `for`
avec utilisation d'un index ou pas. Voici des exemples de code :

```kotlin
for (i in 0..(nameArray.size-1)) {
  println(nameArray[i])
  println(imgArray[i])
}
```

```kotlin
for (i in 0..(mutableList.size-1))
    mutableList.add(MagicCircle(i, i))
```

```kotlin
for (magic in mutableList)
    println(magic.cx)
```

De manière générale, pour un tableau `Array`, l'accès aux éléments
se fait via les crochets : `imgArray[index]` ;
tandis que pour une liste `List`, l'accès aux éléments
se fait via les fonctions `add()` et `get()`.

Envie de pratiquer les tableaux sur _Android_ ? Relevez le challenge du thème "Kotlin"
de l'app. "Kotlin pour Android : quiz".
<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak2-fr.png" alt="liste de cercle magique dans l'app. kotlin pour Android"/>
</div>

{% include aside.html %}

Finalement, le tableau est optimal, au niveau des performances, lorsque les éléments
sont de types primitifs et en nombre fixe. Dans le cas où le nombre d'éléments
est variable, il est préférable d'utiliser une liste.

Cet article met la lumière sur ces syntaxes d'initialisation subtiles
et pas toujours évidentes à retenir.

### {% icon fa-globe %} Références :

1. [Kotlin Array documentation](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-array-list/index.html)
2. <a name="list"></a>[Prefer Collections over older classes](http://www.javapractices.com/topic/TopicAction.do?Id=39)
3. <a name="array"></a>[StackOverFlow: Difference between List and Array types in Kotlin](https://stackoverflow.com/questions/36262305/difference-between-list-and-array-types-in-kotlin)
