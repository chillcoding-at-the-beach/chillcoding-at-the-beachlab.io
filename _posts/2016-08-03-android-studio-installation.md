---
title: "Installer un environnement de développement Android [AK 1 Android Kotlin]"
categories: fr coding tutoriel android
author: macha
last_update: 2023-05-02
---

<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/gallery/thumbs/1_android.svg"
    alt="outils et bonnes pratiques pour développer sur Android en kotlin" width="200px"/>
</div>

Ce tutoriel aborde l'environnement de développement _Android_. En particulier,
il détaille comment installer _Android Studio_, c'est le logiciel officiel,
supporté par Google et basé sur *IntelliJ*, de la société *JetBrains*.

Note : Dans la suite, il conviendra d'utiliser l'abréviation _AS_.

<!--more-->

Pour installer un environnement de développement _Android_, la solution la plus
adéquate est de télécharger _AS_ à partir du site officiel [\[1\]](#android).

#### Est-il possible d'utiliser un logiciel différent d'_Android Studio_ ?

Une alternative est de télécharger seulement le _SDK Android_ (_SDK_ est l'acronyme
de _Software Development Kit_) en utilisant un _IDE_ (_Integrated Development Environment_)
existant comme _Eclipse_ ou _Netbeans_.

#### Quel langage choisir pour développer une application mobile native _Android_ ?

Jusqu'à présent, le langage _Java_ était utilisé pour programmer sous _Android_.
Cela dit, depuis l'annonce officielle du support du langage _Kotlin_ dans _Android_
(cf. [article d'Introduction à Kotlin](/blog/2017/07/11/android-kotlin-introduction/)),
il est recommandé de privilégier ce dernier dans vos développements d'applications _Android_.
En effet, _Kotlin_ nous évite de taper des lignes de code inutiles (_boilerplate_ en anglais).

#### Est-il possible de coder une application _Android_ en _C_ ?

Il est possible d’utiliser du _C_ ou _C++_ avec le _NDK_ (_Native Development Kit_),
**si cela est nécessaire pour l’application**.
Par exemple, une application utilisant de manière intensive
le _CPU_ et n'ayant pas nécessairement une interface utilisateur.
ou bien une application utilisant une bibliothèque existante en _C_.
**Communément, le langage _Kotlin_ est utilisé pour développer une application
_Android_ classique.**

Dans la suite, nous nous concentrerons sur le développement d'application _Android_,
avec le langage _Kotlin_, dans _Android Studio_.

Retrouvez l'intégralité de ce tutoriel dans le thème "ABC d'Android" de l'application "Kotlin pour Android".

<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak1-fr.png" alt="Exploration de l'univers Android"/>
</div>

{% include aside.html %}

## Télécharger Android Studio

1. Allez sur le site de référence Android : [developer.android.com](https://developer.android.com/){:target="_blank"}
2. Dans la partie _Android Studio_ > _DOWNLOAD_, téléchargez _AS_ : _DOWNLOAD ANDROID STUDIO_
3. Acceptez les _Terms and Conditions_ (_I have read and agree with the above terms and conditions_) puis téléchargez :
_DOWNLOAD ANDROID STUDIO FOR ..._
4. Dézipez le fichier téléchargé et suivez les instructions d'installation propre
à votre plateforme _Mac_, _Linux_ ou _Windows_.

Note : Si _Java Development Kit_ n'est pas déjà installé sur votre ordinateur alors
une fenêtre devrait vous suggérer de le faire, suivre les instructions d'installation.

## Configurer avec la dernière version d'Android

Veillez à ne télécharger que la **dernière version du SDK Android**.
Cela afin d'avoir une version à jour et pourquoi pas télécharger l'avant dernière
dans le cas où la dernière n'est pas encore stable...

1. Lancez le logiciel _AS_, suivez les instructions d'installation.
2. _AS_ propose de configurer l'environnement de développement, choisissez
_Standard_ pour une première installation.
3. Acceptez les licences et cliquez sur _Finish_. Le téléchargement des derniers
  paquetages (ici relatifs à la dernière version 11.0, API Level 30) est lancé et peut prendre
  quelques dizaines de minutes.
4. Ne prenez pas de pause &#9749; et travaillez sur votre projet à revenu passif.
5. Allez dans _Configure_ > _SDK Manager_ et
  remarquez les paquetages installés, _SDK Platforms_, en cochant _Show Package Details_. Ici,
  pour l'API 30 :
 * Android SDK Platform 30
 * Sources for Android 30
 * Google APIs Intel x86 Atom System Image (cela correspond à l'émulateur)

Note : Il est également possible d'accéder au _SDK Manager_ via l'icône avec la flèche bleu :
![Icône Android : SDK Manager](/assets/img/post/android/as-sdk.png)

Remarque : Si vous avez choisi une installation _Custom_ au lieu de _Standard_,
vous avez la possibilité de télécharger _HAMX_ (un accélérateur d'émulateur).
Cela dit, son installation nécessite votre présence pour entrer le mot de passe
administrateur.

## Lancer un projet sur un émulateur

Un seul émulateur suffit, c'est-à-dire qu'il n'est pas nécéssaire de télécharger
l'émulateur de chaque nouvelle version.

1. Téléchargez les paquetages nécessaires au lancement d'un émulateur, si cela n'est pas déjà fait :
 * _Google APIs Intel x86 Atom System Image_
2. Re-lancez _AS_ afin que ce téléchargement soit pris en compte.
3. Créez brièvement un projet _Android_ : _+ Create New Project (Empty Activity)_
4. Ouvrez l'_AVD Manager_ disponible à partir de l'icône d'un _smartphone_
avec la mascotte verte.
5. Créez un émulateur : _+ Create Virtual Device..._
6. Optez pour les configurations suivantes, légères :
 * _Select Hardware_: Sélectionnez _Phone 5.1"WVGA_ et cliquez sur _Next_.
 (le téléphone _5.1"WVGA_ est intéressant car sa résolution est ~ 480x800)
 * _Select System Image_: Configurez le système d'exploitation et cliquez sur _Next_.
 * _Android Virtual Device (AVD)_: Optez pour un nom éloquent et cliquez sur _Finish_
6. Lancez l'émulateur ainsi créé via l'icône "flèche verte" situé a côté de celui-ci.

![Icône Android : AVD Manager](/assets/img/post/android/as-avd.png)

## Lancer un projet sur un smartphone

1. Créez brièvement un projet _Android_ : _+ Create New Project (Empty Activity)_
2. Exécutez le projet (en cliquant sur la flèche verte).
3. Choisir de lancer l'application sur
le smartphone branché en USB à votre ordinateur, si le smartphone est bien branché et n'apparait pas :
  * Vérifier l'activation du mode _Débogage USB_
  * Vérifier l'activation du mode développeur.
Note : Pour activer le mode développeur, placez vous dans le menu "A propos" de
votre téléphone, puis cliquez plusieurs fois sur le _Numéro de build_ ou le numéro
de _Version d'Android_ (selon la marque du téléphone).

Il est possible de vérifier la bonne connexion du _smartphone_ via un terminal :
   1. Ouvrez un terminal
   2. Placez vous dans le dossier `platforms-tools/` du _SDK_ (ex : `/Users/macha/Librairy/android/sdk/platforms-tool/`)
   3. Tapez la commande `./adb device`, elle permet d'obtenir la liste des smartphones
   ou tablettes connectées

Note : dans _AS_, afin de connaître le dossier par défaut, dans lequel AS place
les _SDK_, allez dans _File_ > _Project Structure_ > _SDK Location_

Finalement, nous avons créé un projet avec le modèle d'activité vide et l'avons exécuté sur un émulateur
ou _smartphone_. Cela, après avoir installé _AS_ puis téléchargé les paquetages
relatifs aux dernières versions d'_Android_.

Par ailleurs, les 3 icônes à retenir sont :

  * _Run 'app'_ : elle permet l'exécution d'un projet
  * _AVD Manager_ : elle permet de créer, lancer des émulateurs
  * _SDK Manager_ : elle permet de télécharger le dernier _SDK Android_ ou autres

### {% icon fa-globe %} Références

1. <a name="android"></a>[Site officiel Android](https://developer.android.com/index.html)

*[CPU]: Central Processing Unit
*[AS]: Android Studio
