---
title: "Bref, j'ai supprimé gitlab-ci.yml!"
categories: fr coding tutoriel android kotlin gitlab ci
author: macha
permalink: /android-gitlba-ci/
last_update: 2023-04-01
---


<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/gallery/thumbs/3_android-tool.svg"
    alt="outils et bonnes pratiques pour développeur Android" width="200px"/>
</div>

«
— Super : une mise à jour Android API 31, au top! Ça doit être stable après 3 mois...

/> _Exception in thread "main" java.lang.NoClassDefFoundError: javax/xml/bind/annotation/XmlSchema_

— ^^

/> _Caused by: java.lang.ClassNotFoundException: javax.xml.bind.annotation.XmlSchema_

— Ah mince, le CI est cassé !

/> _variables: JAVA_OPTS: "-XX:+IgnoreUnrecognizedVMOptions --add-modules java.xml.bind"_

— Why not!

/> _Unrecognized VM option 'IgnoreUnrecognizedVMOptions --add-modules java.xml.bind'_

— ^- Bref, j'ai supprimé gitlab-ci.yml!»

<!--more-->

# Mettre en place un GitLab CI sur un projet Android [AK 3 Outils Développeur·se]

Cet article explique comment mettre en place un fichier d'intégration continue, _CI_ acronyme de
_Continuous Integration_, sur un projet _Android_ hébergé sur _GitLab_.

En particulier, il est présenté un fichier `.gitlab-ci.yml`, appliqué à un projet _Android_ avec _Kotllin_.
Il convient pour l'_API Android 31_, la version de _JAVA 11 (JDK-11)_, et pour le
dernier outils en ligne de commande _Android_.

## Fichier .gitlab-ci.yml

À partir du fichier exemple fournit par _GitLab_[\[2\]](#gitlabciandroid),
il s'agit de vérifier les variables de versions :
 * `ANDROID_COMPILE_SDK`: correspond à `compileSdkVersion` de votre projet _Android_ (ici 31)
 * `ANDROID_BUILD_TOOLS`: correspond à `buildToolsVersion` (31.0.0)
 * `ANDROID_CMDLINE_TOOLS`: correspond à la dernière version du _command line tool Android_ [\[5\]](#cmdline) (8092744_latest)

Par ailleurs, _Android_ recommande l'utilisation de la variable `ANDROID_SDK_ROOT`
(`ANDROID_SDK_HOME` est déprécié).

Voici un exemple de fichier _.gitlab-ci.yml_ [\[2\]](#gitlabci) :
```yaml
image: openjdk:11-jdk

variables:
  ANDROID_COMPILE_SDK: "30"
  ANDROID_BUILD_TOOLS: "30.0.3"
  ANDROID_SDK_TOOLS: "7583922"

before_script:
  - apt-get --quiet update --yes
  - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1


  - export ANDROID_SDK_ROOT="${PWD}/android-home"
  # Create a new directory at specified location
  - install -d $ANDROID_SDK_ROOT

  - wget --output-document=$ANDROID_SDK_ROOT/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip
  # move to the archive at ANDROID_SDK_ROOT
  - pushd $ANDROID_SDK_ROOT
  - unzip -d cmdline-tools cmdline-tools.zip
  - pushd cmdline-tools
  # since commandline tools version 7583922 the root folder is named "cmdline-tools" so we rename it if necessary
  - mv cmdline-tools tools || true
  - popd
  - popd
  - export PATH=$PATH:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin/

  # Nothing fancy here, just checking sdkManager version
  - sdkmanager --version

  # use yes to accept all licenses
  - yes | sdkmanager --licenses || true
  - sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}"
  - sdkmanager "platform-tools"
  - sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}"

  # Not necessary, but just for surity
  - chmod +x ./gradlew

# Basic android and gradle stuff
# Check linting
lintDebug:
  interruptible: true
  stage: build
  script:
    - ./gradlew -Pci --console=plain :app:lintDebug -PbuildDir=lint

# Make Project
assembleDebug:
  interruptible: true
  stage: build
  script:
    - ./gradlew assembleDebug
  artifacts:
    paths:
      - app/build/outputs/

# Run all tests, if any fails, interrupt the pipeline(fail it)
debugTests:
  interruptible: true
  stage: test
  script:
    - ./gradlew -Pci --console=plain :app:testDebug

### IN THE END EVERYTHING WILL BE OK

```


Aussi, l'image _Docker_ d'_inovex_ [\[10\]](#inovex) fonctionne très bien :
```yaml
image: inovex/gitlab-ci-android

stages:
- release

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

before_script:
- export GRADLE_USER_HOME=$(pwd)/.gradle
- chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
  - .gradle/

build:
    stage: release
    script:
        - ./gradlew clean assembleRelease
    artifacts:
        expire_in: 2 weeks
        paths:
            - app/build/outputs/apk/*.apk
    only:
        - develop
```

## Variables de clé secrète

1. Allez dans _Settings > CI/CD > Variables_, puis _Expand_
2. Ajoutez une variable _KEYS_XML_ avec pour valeurs vos clés secrètes, voici un exemple :
    ```
    <?xml version="1.0" encoding="utf-8"?>
    <resources>
        <string name="base64_encoded_public_key" translatable="false">trucbidule</string>
    </resources>
    ```
3. Ajoutez les clés dans le fichier _XML Android_ depuis _.gitlab-ci.yml_, dans la partie `before_script` :
  ```
  - echo $KEYS_XML > ./app/src/main/res/values/keys.xml
  ```

Mettre un _CI_ sur un projet Android est une bonne pratique,
retrouvez en d'autres dans le thème "Outils pour développeur·se" de l'app.
"Kotlin pour Android : quiz".
<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak3-fr.png" alt="Outils déploiement, test, débogage sur Android"/>
</div>
{% include aside.html %}

Finalement, cet article dévoile un fichier `gitlab-ci.yml`; pour les dernières versions d'_Android_ et indique comment configurer des clés cachés.

### {% icon fa-globe %} Références :

1. <a name="gitlabci"></a>[The ideotec blog: Android GitLab CI Pipeline in 2020](https://blog.ideotec.es/android-gitlab-ci-pipeline-2020/)
2. <a name="gitlabciandroid"></a>[GitLab: Setting up GitLab CI for Android projects](https://about.gitlab.com/blog/2018/10/24/setting-up-gitlab-ci-for-android-projects/)
3. [GitLab: How to publish Android apps to the Google Play Store with GitLab and fastlane](https://about.gitlab.com/blog/2019/01/28/android-publishing-with-gitlab-and-fastlane/)
4. <a name=""></a>[GitLab: Gitlab CI CD documentation](https://docs.gitlab.com/ee/ci/)
5. <a name="cmdline"></a>[developer.android: Android Studio command line tools](https://developer.android.com/studio/index.html)
6. [developer.android: Environment variables](https://developer.android.com/studio/command-line/variables#android_sdk_root)
7. [Google Issue Tracker: About jdk 11 and sdk manager](https://issuetracker.google.com/issues/67495440)
8. <a name="travis"></a>[stackoverflow: Travis CI example](https://stackoverflow.com/questions/53076422/getting-android-sdkmanager-to-run-with-java-11)
9. <a name="jabx"></a> [stackoverflow: JABX Dependencies](https://stackoverflow.com/questions/43574426/how-to-resolve-java-lang-noclassdeffounderror-javax-xml-bind-jaxbexception/43574427#43574427)
10. <a name="inovex"></a> [Inovex: docker img](https://github.com/inovex/gitlab-ci-android)
