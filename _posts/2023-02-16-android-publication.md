---
title: "Mettre en production une application Android [AK 12 Publication]"
categories: fr android google play publication
author: macha
permalink: /android-publication-kotlin/
last_update: 2023-04-28
---


<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/gallery/thumbs/12_android-publication.svg"
  alt="Publication dans l'app. Kotlin pour Android" width="200px"/>
</div>

«
— Super! l'app. "Kotlin pour Android : Quiz" est stable et complète, après 5 ans de développement...

/> _May be It's time to push it to production?_

— Une mise en production à la plage s'impose :)

/> _Are you sure? Is it a good idea?!_

— Ah mince, il y a beaucoup de vent !

/> _Can not hear U, speak louder or change your microphone_

— Why not!

/> _Incredible_

— ^- Bref, j'ai mis en production "Kotlin pour Android" à la plage!»

<!--more-->

Publier une application Android en 3 minutes à la plage, c’est possible :

<iframe width="800" height="450" src="https://www.youtube.com/embed/3t4thAFRGGY" frameborder="0" allowfullscreen></iframe>


## 5 Étapes clés avant la mise en production

1. Faire des tests utilisateurs sur le canal de **Tests ouverts** (2 163 testeurs actifs).
2. Avoir mis en place un processus* intégrant des tests privilégiés sur le canal de **Tests internes**.
3. Préparer une **Fiche Play Store principale**, en français et en anglais, avec les recommandations de la *Google Play Store Listing Certificate*.
4. Mettre en place un **modèle de monétisations** : avec vidéos publicitaires de récompenses ou bien achats intégrés, pour débloquer du contenu.
5. **Finaliser un projet** d’application mobile, d’éducation, avec une mise à jour mensuel (à bi-mensuel) sur 2020-2022.

*Le processus est clair ! Il est juste en cours d’écriture dans une *issue* du [projet GitLab kotlin-for-anroid](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android).

Note :  Merci aux étudiants, pour leurs retours constructifs, encore en cours d’intégration (pas de jeu de mots après 17h17).

Tu peux retrouver les éléments à préparer pour la fiche *Play Store*, dans le thème
"Publication" de l'application en question.
<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak12-fr.png" alt="liste et astuces des éléments de la fiche Play Store d'une app. mobile"/>
</div>
{% include aside.html %}


## {% icon fa-android %} Kotlin pour Android en quelques chiffres clés

Afin d'avoir une vue sur le développement d'une application mobile natif, Android,
voici "Kotlin pour Android : Quiz" en quelques chiffres,

{% icon fa-calendar %} Avant mise en production :
+ Temps de développement : 5 ans à temps partiels
+ Nombre de versions publiées : 50
+ Nombre d'utilisateurs actifs : 131
+ Nombre de téléchargements : 2163
+ Taux de conversion de la fiche Play Store : 57.14%
+ Nombre de Contenus abordés (classés par thèmes Android)  : 12 / 14


## Anomalie de publication ou erreur humaine

🚀 Anomalie dans la date de mise en production

 La véritable date de mise en production n'est pas le 24/01/2023. En fait, comme la mise en production _CHILLcoding_ s'est faite une semaine après la date prévue par la _campagne de pré-enregistrement Google Play Store_, l'application n'était pas disponible dans les pays concernées par le dépassement. Cependant, l'application était disponible dans quelques pays, où la date de mise en production respectait le délai de 3 mois, imposé par la _Google Play Console_. Le temps de se rendre compte de l'anomalie, la mise en production a eu lieu le 20/02/2023! Depuis l'app. "Kotlin pour Android : Quiz" est officiellement en production dans les 177 pays.
Mon premier conseil :

**vérifier la disponibilité de l'app. dans les pays ciblés**.

Note : L'app. peut être indisponible au US ou en France, sur un malentendu, pourtant les voyants sont vert.

🚀 Erreur sur le fichier de clé ou les mots de passes

Il y a fort longtemps, j'avais tout simplement perdu le fichier de clés, en changeant d'ordinateur. À l'époque c'était plutôt simple l'app. concernée n'avait qu'une dizaine de téléchargements. Il a suffit de refaire une publication, conseil :

**conserver précieusement le fichier _Key Store_, le _Key alias_ et les mots de passe**.

Si toi aussi, t'as eu des péripéties de publication ?
N'hésite pas à les partager en commentaire.

Finalement, cet article dévoile les coulisses d'une mise en production fracassante et prometteuse ! La publication n'est pas pour les touristes, il y a quelques éléments de bases à vérifier (disponibilité de l'app. dans les pays concernés, bon fonctionnement de l'app. sur les divers appareils). Aussi, le _Google Play Store_ se reserve le droit d'enlever l'app. si elle ne répond pas à leurs règles. Il s'agit alors d'être réactif and communicate with the support teams with Good Vibes!

### {% icon fa-globe %} Références :

1. [Play Store: Kotlin pour Android : Quiz](https://play.google.com/store/apps/details?id=com.chillcoding.kotlin)
2. [CHILLcoding.com: Kotlin for Android](https://www.chillcoding.com/app/kotlin-for-android/)
3. [youtube.com/@Chillcoding: Kotlin pour Android App](https://www.youtube.com/watch?v=xz6n9_FdMOk)
4. [Kotlin for Android: Send feedback](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/-/issues)
5. [GitLab.com: kotlin-for-anroid](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android)
