---
title: "Faire des premières actions graphiques avec Kotlin dans un projet Android [AK 4 UI Native]"
categories: fr coding tutoriel android kotlin
author: macha
last_update: 2023-04-28
---

<div class="text-center lead" markdown="1">
  <img src="/app/kotlin-for-android/images/gallery/thumbs/4_android-ui.svg"
  alt="Interface Utilisateur Native dans Kotlin pour Android" width="200px"/>
</div>

Ce tutoriel explique comment réaliser des actions graphiques de base avec _Kotlin_,
dans un projet _Android Studio_, d'ores et déjà configuré avec _Kotlin_
(cf. [Configurer _Kotlin_ dans un projet _Android Studio_ [AK 2]][AK-2]).
Le but est de créer une interface graphique simple.
<!--more-->

<div class="text-center lead" markdown="1" >
![GIF demo](/assets/img/post/basic-ui.gif)
</div>

Dans cet article, il est expliqué comment :
  * [Afficher un message court]({{ page.url }}#toast)
  * [Ouvrir une boîte de dialogue]({{ page.url }}#alert)
  * [Lancer une page Internet]({{ page.url }}#browse)
  * [Proposer d'envoyer un email]({{ page.url }}#email)
  * [Proposer de partager du contenu]({{ page.url }}#share)
  * [Lancer un autre écran]({{ page.url }}#start)
  * [Afficher un message d'erreur]({{ page.url }}#log)


Note : Historiquement, avec le langage _Kotlin_, la bibliothèque _Anko Commons_ [\[1\]]({{ page.url }}#anko)
était utilisée pour réaliser des développements classiques liés principalement
à l'interface graphique.
Puis, les incontournables bibliothèques de développement d'application
mobile _Android_ avec le langage _Kotlin_ étaient :
 * _Splitties_ [\[4\]]({{ page.url }}#splitties) : ensemble de bibliothèques _Kotlin_ multiplateformes légères
 * _KTX_ [\[5\]]({{ page.url }}#ktx) : ensemble d'extensions _Kotlin_ soutenues par _Google_

Par ailleurs, une bonne pratique est d'utiliser le _View Binding_ afin d'accéder facilement
aux éléments d'une vue _XML_ (cf. [AK-4]).


##  Afficher un message court <a name="toast"></a>

Afin de présenter des messages d'information court à l'utilisateur, le _SDK Android_
fournit la classe <i style='color:green'>Toast</i> (cf. [documentation officiel](https://developer.android.com/guide/topics/ui/notifiers/toasts.html)).

Avec le _SDK Android_, cela revient à utiliser la classe <i style='color:green'>Toast</i>,
dont l'import correspond à `import android.widget.Toast`, depuis une <i style='color:green'>Activity</i>.
Elle permet de créer un message avec <i style='color:green'>makeText()</i>,
cette dernière prend 3 paramètres :

* le contexte de l'application (`this` depuis une <i style='color:green'>Activity</i>,
`requireActivity` depuis un <i style='color:green'>Fragment</i>)
* le message
* la durée d'affichage long ou court

Enfin, il s'agit d'afficher le message ainsi créé via la fonction <i style='color:green'>show()</i>
(l'appel indispensable à ne pas oublier lorsqu'on code à la plage ;).

```
Toast.makeText(this, "${getString(R.string.text_island)}", Toast.LENGTH_SHORT).show()
```
ou bien alternativement :

```kotlin
Toast.makeText(this, "${getString(R.string.text_island)}", Toast.LENGTH_LONG).show()
```


##  Ouvrir une boite de dialogue <a name="alert"></a>

Avec le _SDK Android_, cela revient à créer une classe <i style='color:green'>DialogFragment</i>
(cf. [documentation Dialogs](https://developer.android.com/guide/topics/ui/dialogs.html)),
puis à l'utiliser depuis une <i style='color:green'>Activity</i> :

```kotlin
class FireMissilesDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.dialog_fire_missiles)
                    .setPositiveButton(R.string.fire,
                            DialogInterface.OnClickListener { dialog, id ->
                                // FIRE ZE MISSILES!
                            })
                    .setNegativeButton(R.string.cancel,
                            DialogInterface.OnClickListener { dialog, id ->
                                // User cancelled the dialog
                            })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
```

Une alternative est de créer la boîte de dialogue, avec <i style='color:green'>AlertDialog</i> directement dans l'<i style='color:green'>Activity</i>.

Retrouvez le code dans le thème "Interface Utilisateur Native" de l'application "Kotlin pour Android".
<div class="text-center lead" markdown="1" >
  <img src="/app/kotlin-for-android/images/k4a-ak4-alert.png" alt="Alert Dialog sous Android avec kotlin"/>
</div>
{% include aside.html %}

## Ouvrir une page Internet <a name="browse"></a>

Afin d'ouvrir une page Internet avec le navigateur natif, il s'agit d'utiliser
une intention fournit par le _SDK Android_, tout comme le lancement d'un second écran.

Plus précisément, cela revient à utiliser la classe <i style='color:green'>Intent</i>,
comme suit, depuis une <i style='color:green'>Activity</i> :

```kotlin
val url = "https://www.chillcoding.com/"
val intent = Intent(Intent.ACTION_VIEW)
intent.data = Uri.parse(url)
startActivity(i)
```

1. Créez la fonction <i style='color:#00bfff'>browse()</i> :

        private fun browse(url: String) {
          var browser = Intent(Intent.ACTION_VIEW, Uri.parse("https://"+url))
          startActivity(browser)
        }

2. Appelez la fonction ainsi créée sur un bouton :

        findViewById<Button>(R.id.mainBtn).setOnClickListener { browse("www.chillcoding.com") }


## Proposer d'envoyer un email <a name="email"></a>

Afin  de proposer d'envoyer un email, à l'utilisateur, avec une application native,
le _SDK Android_ fournit la classe <i style='color:green'>Intent</i> avec un paramétrage spécifique.

Plus précisément, cela revient à utiliser la classe <i style='color:green'>Intent</i>,
instanciée de la façon suivante :

```kotlin
val intent = new Intent(Intent.ACTION_SENDTO);
intent.type = "text/plain"
intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("hello@chillcoding.com"))
intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject_feedback))
intent.putExtra(Intent.EXTRA_TEXT, "")

startActivity(Intent.createChooser(intent, "Send Email"))
```

1. Créez la fonction <i style='color:#00bfff'>sendEmail()</i> :

        private fun sendEmail(to: String, subject: String, msg: String) {
          val emailIntent = Intent(Intent.ACTION_SEND)

          emailIntent.data = Uri.parse("mailto:")
          emailIntent.type = "text/plain"
          emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
          emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
          emailIntent.putExtra(Intent.EXTRA_TEXT, msg)

          try {
              startActivity(Intent.createChooser(emailIntent, getString(R.string.title_send_email)))
          } catch (ex: ActivityNotFoundException) {
              toast(R.string.text_no_email_client)
          }
        }

2. Appelez la fonction ainsi créée sur un bouton :

        sendEmail("macha@chillcoding.com", "Hi", "Hello!")


## Proposer de partager du contenu <a name="share"></a>

Afin de proposer de partager du contenu via les applications disponibles, installées
au préalable par l'utilisateur (_Slack_, _Messenger_, etc.), le _SDK Android_ fournit
la classe <i style='color:green'>Intent</i> avec un paramétrage spécifique.

Plus précisément, cela revient à utiliser la classe <i style='color:green'>Intent</i>
de la façon suivante :

```kotlin
val intent = Intent(Intent.ACTION_SEND)
intent.type = "text/plain"
intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.text_share_app))

startActivity(Intent.createChooser(intent, "Share"))
```


1. Dans un nouveau fichier `Tool.kt`, implémentez la fonction d'extension :
```kotlin
fun Context.share(text: Int, subject: Int = R.string.app_name): Boolean {
    try {
        val intent = Intent(android.content.Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(subject))
        intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(text))
        startActivity(Intent.createChooser(intent, null))
        return true
    } catch (e: ActivityNotFoundException) {
        e.printStackTrace()
        return false
    }
}
```

2. Appelez la fonction <i style='color:#00bfff'>share()</i> :

        share(R.string.text_share, R.string.title_share)


## Lancer un autre écran <a name="start"></a>

Afin d'empiler un second écran dans la pile d'écrans, le _SDK Android_ fournit
la classe <i style='color:green'>Intent</i> (cf. [documentation officiel](https://developer.android.com/training/basics/firstapp/starting-activity.html)),
représentant une intention, à combiner avec <i style='color:green'>startActivity()</i>.

Avec le _SDK Android_, cela revient à utiliser la classe <i style='color:green'>Intent</i>,
dont l'import correspond à `import android.content.Intent`, depuis une <i style='color:green'>Activity</i> :

```kotlin
val intent = Intent(this, MySecondaryActivity::class.java)
intent.putExtra("id", 3)
intent.putExtra("name", "Macha")
startActivity(intent)
```

Note : Les données peuvent être récupérées via `arguments.getExtra("givenkey")`
(soit `arguments.getExtra("id"`) depuis la seconde <i style='color:green'>Activity</i>.

Une alternative est d'utiliser la bibliothèque _navigation_ de _Jetpack_ afin de gérer
l'enchainement des écrans de l'application mobile _Android_ (cf. [d.android.com: Navigation for responsive UIs](https://d.android.com/jetpack/compose/nav-adaptive)).

## Afficher un message d'erreur <a name="log"></a>

Afin d'afficher des messages dans la console du développeur _Logcat_, soit dans la console
_Android Monitor_, le _SDK Android_ fournit la classe <i style='color:green'>Log</i>
(cf. [documentation officiel](https://developer.android.com/reference/android/util/Log.html)).
Il s'agit d'utiliser les méthodes <i style='color:green'>Log.i()</i>, <i style='color:green'>Log.e()</i>, etc. Elles prennent 2 paramètres :

* un _TAG_, une chaine de caractères comprenant le nom de la classe courante
* le message à afficher dans la console

Avec le _SDK Android_, cela revient à utiliser la classe <i style='color:green'>Log</i>
dont l'import correspond à `import android.util.Log` :

```kotlin
 Log.i(MainActivity::class.simpleName, "Kotlin is an island")
```


## Retour d'expérience

Finalement, ces implémentations peuvent être codées via des fonctions d'extensions _Kotlin_.
En particulier, cela permet de programmer très rapidement les tâches utilisateurs les plus courantes :

* [Voir un message rapide]({{ page.url }}#toast)
* [Interagir avec une boîte de dialogue]({{ page.url }}#alert)
* [Ouvrir un autre écran]({{ page.url }}#start)
* [Voir une page Internet]({{ page.url }}#browse)
* [Envoyer un email]({{ page.url }}#email)
* [Partager du contenu]({{ page.url }}#share)

La connaissance du _SDK Android_ est alors bienvenue.
Aussi, l'implémentation de ses propres fonctions d'extensions peuvent être restreintes
à un contexte spécifique, c'est-à-dire qu'elles peuvent être appelées depuis
une <i style='color:green'>Activity</i> (`context`)
ou bien depuis un <i style='color:green'>Fragment</i> (`requireContext`).

Par ailleurs, utiliser une bibliothèque c'est cool, cela dit si elle n'est pas soutenue par
_Google_ alors cela peut tomber à l'eau... En effet, mettre à jour une bibliothèque
est fastidieux.

Aussi, _Google_ soutient des nouveautés et ces dernières ne sont pas
immédiatement adoptées par la communauté. Par exemple, le langage _Kotlin_
a pris plusieurs années avant d'avoir une autorité et prendre la place méri†ée.

_Compose_ c'est de la bombe cela dit il n'est pas intéressant de convertir les UIs
_XML_ des anciens projets en _@Composable_. Cependant avec l'intéropérabilité
il est possible de mixer _Compose_ et interface graphique _XML_.

Pour finir, il est important de savoir ce qu'il y a sous le capot (`Cmd` ou `Ctrl` + clique :)
afin de voir les implémentations originales.
Parfois, il est nécessaire de mettre les mains dans le cambouis afin de développer
une fonctionnalité plus évoluée, voir plus complexe, voir plus adaptée à ses besoins.
L'existant est alors une source d'inspiration.


### {% icon fa-globe %} Références :

1. <a name="anko"></a>[GoodBye Anko on GitHub](https://github.com/Kotlin/anko/blob/master/GOODBYE.md)
2. <a name="theme"></a>[Setting up a Material Components theme for Android](https://medium.com/over-engineering/setting-up-a-material-components-theme-for-android-fbf7774da739)
3. [Choose an open source license](https://choosealicense.com/)
4. <a name="splitties"></a>[Splitties: All the Android splits](https://github.com/LouisCAD/Splitties)
5. <a name="ktx"></a>[Android KTX, Part of Android Jetpack.](https://developer.android.com/kotlin/ktx)


*[AS]: Android Studio
[AK-2]: https://www.chillcoding.com/blog/2017/09/28/configurer-kotlin-projet-android/
[AK-4]: https://www.chillcoding.com/blog/2021/03/30/utiliser-view-binding-kotlin/
