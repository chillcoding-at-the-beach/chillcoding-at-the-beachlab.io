---
layout: blog
title: Formations | ChillCoding
---

# Formation Dev. Mobile : Développer une Application Native Android avec le langage Kotlin

{: style="text-align:center"}
![Android View](/assets/img/post/android-formation.png)

Cette formation s’adresse aux personnes scientifiques souhaitant se lancer
dans le **développement d’application mobile sur _Android_ avec le langage _Kotlin_**.
En particulier, les développeur·se·s ayant déjà une connaissance de la programmation
orientée objet ou fonctionnelle permettront d'aller plus loin dans ladite formation.

L’apprentissage se fait via des **démonstrations interactives de codage** et des exercices pratiques
réalisés sous _Android Studio_.

Note : Des quiz sont prévus en vue de préparer des certifications reconnues.

Tout au long de la formation, afin d’augmenter sa productivité, il est indiqué
des astuces pour utiliser au mieux les outils de développement
(raccourcis clavier, auto-génération de code, bibliothèques, références, etc.).

De plus, les **bonnes pratiques de développement** seront mises en lumière (_Design pattern_, architecture _Model View ViewModel_).

A l'issue de la formation les participant·e·s sauront développer, tester et déployer une application mobile sur terminaux <i>Android</i>. Elle permettra de découvrir l’écosystème Android afin d’être autonome dans la réalisation d’applications de la phase de conception à la publication.

## Durée
3 à 4 jours

La formation est basée sur un tronc commun se déroulant sur 3 jours
(des thèmes supplémentaires peuvent être sélectionnés à la carte).

## Pré-requis
- Expérience dans un langage de Programmation, Orientée Objet ou Fonctionnelle

## Objectifs pédagogiques
* Configurer un environnement de développement *Android* à jour
* Comprendre et écrire du code *Kotlin*
* Connaître les principales bibliothèques du monde *Android*
* Savoir créer une interface graphique
* Développer une application *Android* en respectant les bonnes pratiques
* Travailler avec des bases de données et services web

## Jour 1 : Android et Kotlin

#### ABC d’Android
<ol type="A">
  <li>Plateforme <i>Android</i></li>
  <li>Environnement de développement</li>
  <li>Principes de programmation</li>
</ol>

Supports en ligne : [AK-1A: Android en bref], [AK-1B: Installation Android]

#### Pratique : Créer un premier projet 'Hello Android'
- Arborescence du projet, fichiers clés
- Déploiement sur émulateur et appareil physique
- Exploration du cycle de vie de l'*Activity*
- Gestion du clique sur une image (accès aux éléments de la vue)
<br>
<br>

#### Langage Kotlin pour Android
<ol type="A">
 <li>Concepts du langage Kotlin</li>
 <li>Classe, propriété, fonction</li>
 <li>Variable, opérateur, condition</li>
 <li>Programmation fonctionnelle</li>
</ol>

Supports en ligne : [AK-2A: Kotlin en bref], [AK-2B: Vue personnalisée], [AK-2C: Tableau Kotlin]

🎓Formation en ligne :
<a  class="header-content-inner" href="https://formation.chillcoding.com/courses/100-kotlin" class="btn btn-primary btn-xl page-scroll">100% Langage KOTLIN</a>

#### Pratique : Créer une vue personnalisée
* Héritage et constructeurs
* Création d'objet via *data class*
* Initialisations de variables
* Manipulation des listes et tableaux
<br>
<br>

## Jour 2 : UI et persistance

#### Interface Utilisateur Native
<ol type="A">
  <li><i>Material Design</i></li>
  <li>Vue et agencements</li>
  <li>Ressources (image vectorielle, thème, internationalisation, dimension, etc.)</li>
  <li>Composants graphiques natifs</li>
</ol>

Supports en ligne : [AK-4D: Bibliothèques graphiques Kotlin], [AK-4B: Binding kotlin], [AK-4: UI Cheatsheet]

#### Pratique : Créer une interface utilisateur élaborée
* Manipulation du *ConstraintLayout*
* Mise en pratique des principaux composants graphiques (*CardView*)
* Gestion du mode portrait, paysage
* Ajout d’une langue
<br>
<br>

#### Menus
<ol type="A">
  <li>Composants graphiques de Navigation</li>
  <li>Navigation entre écrans</li>
  <li>Notion d'architecture mobile</li>
</ol>

#### Pratique : Créer une application structurée
* Mise en pratique d'un des menus
* Mise en place de l'architecture _**MVVM**_
<br>
<br>

#### Affichage d'une liste d'éléments
<ol type="A">
  <li>Principe d'adaptateur <i>Android</i></li>
  <li>Exemples de vue</li>
  <li>Implémentation du <i>RecyclerView</i></li>
</ol>

Support en ligne : [AK-6: RecyclerView]

#### Pratique : Afficher une liste d’élément
* Affichage de la liste des plus belles plages
<br>
<br>

#### Persistance des Données
<ol type="A">
  <li>Stockage de paires clé-valeur</li>
  <li>Système de fichiers</li>
  <li>Base de données (BDD) <i>SQLite</i></li>
  <li><i>Object Relationnel Mapping</i> (ORM) : Bibliothèque <i>Room</i></li>
</ol>

Supports en ligne : [AK-7: SharedPreferences], [AK-8: BDD]

#### Pratique : Manipuler une BDD dans un Thread parallèle
* Utilisation de [DataStore](https://developer.android.com/topic/libraries/architecture/datastore) pour enregistrer des données légères
* Gestion d'une BDD locale avec _Room_
<br>
<br>

## Jour 3 : Persistance et Communication HTTPS

#### Communication Réseau
<ol type="A">
  <li>Contexte d’échange</li>
  <li>Traitement en tâche de fond, <i>Coroutines</i></li>
  <li>Communication HTTPS avec <i>Retrofit</i></li>
</ol>

Supports en ligne : [AK-9: Retrofit GET], [AK-9B: Retrofit POST]

#### Pratique : Consommer un Service Web distant
* Consommation d’un service web avec  _Retrofit_
<br>
<br>

#### Outils pour Développer
<ol type="A">
  <li>Messages systèmes et console <i>Logcat</i></li>
  <li>Débogage via les points d’arrêt</li>
  <li>Déploiement et tests</li>
  <li>Bibliothèques et références</li>
</ol>

Supports en ligne : [AK-3D: Android References], [AK-3: Raccourcis clavier]

#### Pratique : Importer un projet exemple
* Exploration des projets exemples
* Analyse d'une application (mémoire, code, interface graphique, etc.)
* Compatibilité des versions et évolution
<br>
<br>

**Déclaration d'activité enregistrée sous le numéro 93060907806 du préfet de région de Provence-Alpes-Côte d'Azur.**

## La formatrice

![formation](https://gravatar.com/avatar/60d8d9b39be4c97c111c10867f24ca7f?s=100) Ingénieur en développement mobile depuis 2010, [Macha DA COSTA](https://twitter.com/MachaDaCosta) s’est spécialisée dans la conception, le développement et la publication d’applications mobiles Android. En tant qu'indépendante, elle enseigne à l’[Université de Nice Sophia-Antipolis](https://univ-cotedazur.fr/formation/offre-de-formation/licence-pro-metiers-de-linformatique-conception-developpement-et-test-de-logiciels-parcours-dam) et chez [Ynov](https://www.ynov-aix.com/). De plus, elle accompagne ses clients dans leurs projets mobiles ou web.

N'hésitez pas à prendre rendez-vous <a href="mailto:{{ site.data.members.macha.email }}" title="{{ site.data.members.macha.email }}">
  <i class="fa fa-envelope-o fa-x" data-wow-delay=".2s"></i> {{ site.data.members.macha.email }}
</a> pour un programme personnalisé.

### Application de la formation :
{% include aside.html %}

### 🎓Formation en ligne, en cours...
<a href="https://formation.chillcoding.com/courses/developper-une-application-mobile-native-android-avec-kotlin" class="btn btn-primary btn-xl page-scroll">Développer une application mobile native Android avec Kotlin</a>

## Jour 4 : à la carte

#### Réussir une Publication sur le PlayStore
Introduction à l’optimisation sur les boutiques en ligne d'application
(<i>App Store Optimization</i>, <i>ASO</i>), la console de publication, et les statistiques.
<ol type="A">
  <li>Publication</li>
  <li>Google Play Store Listing</li>
  <li>Monétisation</li>
</ol>

#### Pratique : Mettre en place un In-app purchase
* Configuration de la console _Google Play_
* Utilisation de la bibliothèque _Google Play Billing 4_
<br>
<br>

#### Multiplateforme avec Kotlin
<ol type="A">
  <li>Application multiplateforme native</li>
  <li>Concept de bibliothèque <i>Kotlin</i>, pour <i>Android</i> et <i>iOS</i></li>
  <li>Architecture de l'environnement de développement</li>
</ol>

#### Pratique : Créer un premier projet multiplateforme
* Configuration de l'environnement de développement
* Création d’une première bibliothèque partagée
<br>
<br>

#### Multimédia
<ol type="A">
  <li>Accéléromètre</li>
  <li>Son</li>
  <li>Utiliser d’autres applications comme la Camera</li>
</ol>

#### Pratique : Créer un jeu
* Animation graphiques et sonores d’éléments
* Utilisation de l’accéléromètre pour animer un élément graphique
* Prise d’une photo avec l’application native
* Ouverture de l’application des paramètres
<br>
<br>

#### Géolocalisation et Cartographie
<ol type="A">
  <li>Géolocalisation</li>
  <li>Carte géographique avec l'<i>API Google Maps</i></li>
</ol>

#### Pratique : Afficher la localisation de l’utilisateur
* Utilisation des services _Google Maps_ dans une *Activity*
*  Affichage de la dernière position détectée
<br>
<br>

#### Montre Connectée avec Android Wear
<ol type="A">
  <li>Interface et ses différents modes</li>
  <li>Configuration d’un projet <i>Android Wear</i></li>
  <li>Communication des données entre les modules</li>
</ol>

#### Pratique : Créer un premier projet sur Android Wear
* Déploiement sur émulateur et sur montre
<br>
<br>

#### Pratique : Approfondissement d'un développement classique
* Ouverture d’un document PDF stocké en local
* Enregistrement d’une image dans un fichier
* Utilisation de *Fragment* avec *ViewPager* (menu à onglets)
* Communication d’information entre *Fragment*
<br>
<br>

## À préparer
* Ordinateur personnel
* Android Studio avec dernière version du SDK Android ([lien de téléchargement](https://developer.android.com/studio/index.html))
* Téléphone *Android*, avec câble de connexion

## Plan
[Programme de formation détaillé (PDF)](https://www.chillcoding.com/assets/pdf/programme-formation-android-kotlin.pdf)


[AK-1A: Android en bref]:https://www.chillcoding.com/blog/2014/08/08/extraordinaire-humanoide/
[AK-1B: Installation Android]:https://www.chillcoding.com/blog/2016/08/03/android-studio-installation
[AK-2A: Kotlin en bref]:https://www.chillcoding.com/blog/2017/07/11/android-kotlin-introduction/
[AK-2B: Vue personnalisée]:https://www.chillcoding.com/android-custom-view-heart/
[AK-2C: Tableau Kotlin]:https://www.chillcoding.com/blog/2019/09/26/kotlin-array/
[AK-3D: Android References]:https://www.chillcoding.com/blog/2017/01/27/android-references
[AK-3: Raccourcis clavier]:https://www.chillcoding.com/blog/2016/04/01/android-top-raccourcis/
[AK-4D: Bibliothèques graphiques Kotlin]:https://www.chillcoding.com/blog/2017/10/09/utiliser-bibliotheque-graphique-kotlin-android/
[AK-4B: Binding kotlin]:https://www.chillcoding.com/blog/2021/03/30/utiliser-view-binding-kotlin/
[AK-4: UI Cheatsheet]:https://www.chillcoding.com/blog/2017/01/16/android-ui-cheatsheet/
[AK-6: RecyclerView]:https://www.chillcoding.com/blog/2018/10/22/creer-liste-recyclerview-kotlin-android/
[AK-7: SharedPreferences]:https://www.chillcoding.com/blog/2014/10/10/utiliser-fichier-preferences/
[AK-8: BDD]:https://www.chillcoding.com/blog/2018/01/17/creer-bdd-sqlite-kotlin-android/
[AK-9: Retrofit GET]:https://www.chillcoding.com/blog/2017/03/14/requete-http-get-retrofit-android/
[AK-9B: Retrofit POST]:https://www.chillcoding.com/blog/2015/11/16/requete-http-post-retrofit-android/
